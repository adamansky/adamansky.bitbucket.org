package ru.nsu.sample

fun wrapGreeting(block: () -> String) {
    print("Hello ${block()}!!!")
}

fun main() {
    wrapGreeting {
        "Andy"
    }
    wrapGreeting {
        readLine()!!
    }
}

// Console: Hello Andy!!!