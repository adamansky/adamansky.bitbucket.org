package ru.nsu

fun runBlock(block: () -> Unit) {
    block()
}

fun main(args: Array<String>) {
    //var zz = "test"
    runBlock {
        //zz = "a"
        println("Hello!")
    }

//    lock {
//        println("Lock1");
//    }
//
//    lock2 {
//        println("Lock2");
//    }
}

//inline fun <T> lock(body: () -> T): T {
//    return body()
//}
//
//fun <T> lock2(body: () -> T): T {
//    return body()
//}


