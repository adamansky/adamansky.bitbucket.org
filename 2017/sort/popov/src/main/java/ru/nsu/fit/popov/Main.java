package ru.nsu.fit.popov;

import ru.nsu.fit.popov.sorter.Starter;

public class Main {

    /**
     * @param args      { inputPath, outputPath }
     */
    public static void main(String[] args) {
       if (args.length < 2) {
           error();
           return;
       }

       try {
           new Starter(args[0], args[1]).start();
       } catch (Exception e) {
           e.printStackTrace();
       }
    }

    private static void error() {
        System.err.println("java -Xms1G -Xmx1G -jar filesorter.jar inputPath outputPath");
    }
}
