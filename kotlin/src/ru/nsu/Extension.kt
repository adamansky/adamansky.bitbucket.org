package ru.nsu

class MyClass(val i: Int) {
    fun second() {
        this.printMe("Hello");
    }
}

fun MyClass.printMe(value: String) {
    println("$value ${this.i}");
}

fun main(args: Array<String>) {
    val v = MyClass(42)
    v.second()
}














