package chereshnev;

import java.util.*;
import java.util.concurrent.*;

class ArrayHelper
{
	public static final int MAX_THREADS = Utils.getProcessors();
    private static ThreadPool threadPool;
	
	private static int binarySearch(int[] array, int lo, int hi, int v)
	{
		int m = (lo + hi)/2;
		if (array[m] == v)
			return m;
		if (array[m] < v)
			return  (hi < m + 1) ? -1 : binarySearch(array, m + 1, hi, v);
		return (m - 1 < lo) ? -1 : binarySearch(array, lo, m - 1, v);
	}
	
	public static int binarySearch(int[] array, int v)
	{
		return binarySearch(array, 0, array.length - 1, v);
	}
	
	public static int interpolationSearch(int[] array, int v)
	{
		int lo = 0;
		int hi = array.length - 1;
		while (array[lo] <= v && array[hi] >= v)
		{
			int m = (int)(lo + (((long)v - array[lo]) * (hi - lo)) / ((long)array[hi] - array[lo]));
			if (array[m] < v)
				lo = m + 1;
			else if (array[m] > v)
				hi = m - 1;
			else
				return m;
		}
		return (array[lo] == v) ? lo : -1;
	}
	
	public static int countOccurences(int[] array, int v)
	{
		int count = 0;
		long t = System.nanoTime();
		int fi = interpolationSearch(array, v);
		for (int i = fi; i < array.length && array[i] == v; i++)
			count++;
		for (int i = fi - 1; i >= 0 && array[i] == v; i--)
			count++;
		return count;
	}
	
	private static void swapIfGreater(int[] array, int a, int b)
	{
		if (array[a] > array[b])
		{
			int t = array[a];
			array[a] = array[b];
			array[b] = t;
		}
	}

	private static final void swap(int[] array, int i, int j)
	{
		int t = array[i];
		array[i] = array[j];
		array[j] = t;
	}

	private static void insertionSort(int[] array, int lo, int hi)
	{
		for (int i = lo; i < hi; i++)
		{
			int num = i;
			int t = array[i + 1];
			while (num >= lo && t < array[num])
			{
				array[num + 1] = array[num];
				num--;
			}
			array[num + 1] = t;
		}
	}

	private static int pickPivotAndPartition(int[] array, int lo, int hi)
	{
		int num = lo + (hi - lo) / 2;
		swapIfGreater(array, lo, num);
		swapIfGreater(array, lo, hi);
		swapIfGreater(array, num, hi);
		int t = array[num];
		swap(array, num, hi - 1);
		int i = lo;
		int num2 = hi - 1;
		while (i < num2)
		{
			while (array[++i] < t)
				;
			while (t < array[--num2])
				;
			if (i >= num2)
				break;
			swap(array, i, num2);
		}
		swap(array, i, hi - 1);
		return i;
	}

	private static void introSort(final int[] array, int lo, int hi)
	{
		//optimized concurrent quick sort(from .NET sources)
		while (hi > lo)
		{
			int num = hi - lo + 1;
			if (num <= 32)
			{
				if (num == 1)
					return;
				if (num == 2)
				{
					if (array[lo] > array[hi])
					{
						int t = array[lo];
						array[lo] = array[hi];
						array[hi] = t;
					}
					return;
				}
				if (num == 3)
				{
					swapIfGreater(array, lo, hi - 1);
					swapIfGreater(array, lo, hi);
					swapIfGreater(array, hi - 1, hi);
					return;
				}
				insertionSort(array, lo, hi);
				return;
			}
			final int num2 = pickPivotAndPartition(array, lo, hi);
			final int hiCopy = hi;
			final int THRESHOLD = 1000;
			if (hiCopy - num2 > THRESHOLD)
				threadPool.execute(new Runnable()
				{
					public void run()
					{
						introSort(array, num2 + 1, hiCopy);
					}
				});
			else
				introSort(array, num2 + 1, hiCopy);
			hi = num2 - 1;
		}
	}
	
	public static void introSort(final int[] array)
	{
		threadPool = new ThreadPool(4*MAX_THREADS);
		threadPool.execute(new Runnable()
		{
			public void run()
			{
				introSort(array, 0, array.length - 1);
			}
		});
		threadPool.waitDone();
	}
}