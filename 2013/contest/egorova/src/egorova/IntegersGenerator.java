package egorova;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: vvego_000
 * Date: 25.11.13
 * Time: 0:39
 * To change this template use File | Settings | File Templates.
 */
public class IntegersGenerator {
    private int[] keyIntegersArray = null;
    private int keysNumber = 0;
    private int integersNumber = 0;
    public IntegersGenerator(int pIntegersNumber, int pKeysNumber)
    {
        keysNumber = pKeysNumber;
        integersNumber = pIntegersNumber;
    }
    public int[] getKeys() {
        return keyIntegersArray;
    }

    public void generateRandomNumbers(String fileName) throws Exception {

        int[] integersArray = new int[integersNumber];
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < integersNumber; i++) {
            integersArray[i] = random.nextInt(Integer.MAX_VALUE);
        }
        keyIntegersArray = Arrays.copyOf(integersArray, keysNumber);
        Arrays.sort(integersArray);
        write(integersArray, fileName);
    }
    void write(int[] integersArray, String fileName) throws  Exception
    {
        FileChannel fileChannel = new RandomAccessFile(fileName, "rw").getChannel();
        ByteBuffer buf = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, 4 * integersNumber);
        for (int i : integersArray) {
            buf.putInt(i);
        }
        fileChannel.close();
    }
}