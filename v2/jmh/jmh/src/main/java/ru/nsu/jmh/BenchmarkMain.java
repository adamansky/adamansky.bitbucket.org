package ru.nsu.jmh;

import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class BenchmarkMain {
  public static void main(String[] args) throws RunnerException {
    final Options options = new OptionsBuilder().warmupIterations(1).measurementIterations(2)
        .include(MyBenchmark.class.getSimpleName())
        // .addProfiler(StackProfiler.class)
        .forks(1).build();
    new Runner(options).run();
  }
}