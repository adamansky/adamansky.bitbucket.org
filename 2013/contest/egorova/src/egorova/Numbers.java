package egorova;

public class Numbers {

    public static void main(String args[]) throws Exception {
        if (args.length == 4) {
            int integersNumber = Integer.valueOf(args[0]);
            int keysNumber = Integer.valueOf(args[2]);
            String integersFileName = args[1];
            String tableFileName = args[3];
            long startTime = System.currentTimeMillis();
            IntegersGenerator generator = new IntegersGenerator(integersNumber, keysNumber);
            generator.generateRandomNumbers(integersFileName);
            FrequencyTable table = new FrequencyTable(integersNumber, keysNumber);
            table.generateFrequencyTable(integersFileName, tableFileName, generator.getKeys());
            long stopTime = System.currentTimeMillis() - startTime;
            System.out.println("T=" + stopTime);
        } else {
            System.out.println("Incorrect input: <integers_number> <integers_file_name> <keys_number> <table_file_name>");
        }
    }
}