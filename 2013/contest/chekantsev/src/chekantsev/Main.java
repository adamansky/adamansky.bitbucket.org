package chekantsev;
import java.io.*;
import java.util.*;


public class Main {
    static  int MAX_INT = Integer.MAX_VALUE;
    static List<Integer> list = new ArrayList<Integer>();
    static HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();



    public static void main(String [] args) throws IOException {
        long start = System.currentTimeMillis();
        Random rnd = new Random();
        for (int i = 0; i < 10000000; i++)
        {
            list.add(rnd.nextInt(MAX_INT) - rnd.nextInt(MAX_INT));
        }

        Collections.sort(list);
        for (int j = 0; j < 100000; j++)
        {
            hm.put(list.get(rnd.nextInt(10000000)), 0);
        }
        FileOutputStream fos = new FileOutputStream("Sorted.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);




        for (Integer i : list)
        {
            oos.writeInt(i);
        }
        list.clear();
        oos.close();

        FileInputStream fis = new FileInputStream("Sorted.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);

        for (int i = 0; i < 10000000; i++)
        {
            list.add(ois.readInt());
        }

        PrintWriter pw = new PrintWriter(new File("Keys.txt"));
        for (Integer i : list)
        {
            Integer elem = hm.get(i);
            if (elem != null)
            {
                hm.put(i, elem == 1 ? 1 : elem + 1);
                pw.println(i + " = " + hm.get(i));

            }

        }
        //pw.println(hm);
        pw.close();
        ois.close();
        System.out.println("T= " + (System.currentTimeMillis() - start));
    }
}