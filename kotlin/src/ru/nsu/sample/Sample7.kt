package ru.nsu.sample

class Empty

class Person(val lastName: String, val firstName: String, val age: Int)

class Device {

    private val serial: String

    constructor(serial: String) {
        this.serial = serial
    }

    init {
        // Default constructor initializer
    }

    fun getInfo(): String {
        return "My serial: $serial"
    }
}

class Device2(private val serial: String) {
    fun getInfo() = "My serial: $serial"
}
