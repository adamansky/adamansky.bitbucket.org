package li;

import java.util.*;
import java.io.*;

public class Main
{
    private static Random random = new Random();
    private static int N = 10000000;
    private static int KEYN = 100000;

    private static Thread sort(final int[] numbers, int fromIndex, int toIndex, int depth) throws Exception
    {
        //custom partition to depth MAX_DEPTH and sort with Arrays.sort in separate thread
        final int MAX_DEPTH = 2;
        final int fi = fromIndex;
        final int ti = toIndex;
        if (depth == MAX_DEPTH)
        {
            Thread thread = new Thread(new Runnable()
            {
                public void run() { Arrays.sort(numbers, fi, ti + 1); }
            });
            thread.start();
            return thread;
        }
        int x = (numbers[fi] + numbers[ti])/2;
        while (fromIndex <= toIndex)
        {
            while (numbers[fromIndex] < x) fromIndex++;
            while (numbers[toIndex] > x) toIndex--;
            if (fromIndex <= toIndex)
            {
                int tmp = numbers[fromIndex];
                numbers[fromIndex] = numbers[toIndex];
                numbers[toIndex] = tmp;
                fromIndex++;
                toIndex--;
            }
        }
        Thread t1 = null;
        Thread t2 = null;
        if (fromIndex < ti)
            t1 = sort(numbers, fromIndex, ti, depth + 1);
        if (toIndex > fi)
            t2 = sort(numbers, fi, toIndex, depth + 1);
        if (t1 != null) t1.join();
        if (t2 != null) t2.join();
        return null;
    }

    public static Map<Integer, Integer> part1() throws Exception
    {
        final int[] numbers = new int[N];
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        for (int i = 0; i < N; i++)
            numbers[i] = random.nextInt();
        for (int i = 0; i < KEYN; i++)
            map.put(numbers[i], 0);

        sort(numbers, 0, N - 1, 0);

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("lout"));
        oos.writeObject(numbers);
        oos.close();
        return map;
    }

    public static void part2(Map<Integer, Integer> keyMap) throws Exception
    {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("lout"));
        int[] numbers = (int[])ois.readObject();
        ois.close();

        BufferedWriter bw = new BufferedWriter(new PrintWriter("keys"), 32000);
        for (Map.Entry<Integer, Integer> e : keyMap.entrySet())
        {
            int value = e.getKey();
            int index = Arrays.binarySearch(numbers, 0, N, value);
            int count = 0;
            for (int i = index; i >= 0 && numbers[i] == value; i--, count++);
            for (int i = index; i < N && numbers[i] == value; i++, count++);
            keyMap.put(value, count - 1);
        }
        for (Map.Entry<Integer, Integer> e : keyMap.entrySet())
        {
            bw.write(Integer.toString(e.getKey()) + " : " + e.getValue());
            bw.newLine();
        }
        bw.close();
    }

    public static void main(String[] args) throws Exception
    {
        for (int i = 0; i < 5; i++)
        {
            long t = System.currentTimeMillis();
            part2(part1());
            System.out.println("Iteration#" + i + " " + (System.currentTimeMillis() - t));
        }
    }
}