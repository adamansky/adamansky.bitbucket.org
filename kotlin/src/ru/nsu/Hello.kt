package ru.nsu

//val s = "test"

/**
 * Kotlin sample class
 */
data class Sample1(val x: Int)

class Sample2 {
    var x: String? = null
    @JvmField
    var y: Int? = null
}

fun main(args: Array<String>) {
    println("Hello, world!")
}