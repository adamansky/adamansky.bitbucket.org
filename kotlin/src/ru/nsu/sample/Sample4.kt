package ru.nsu.sample

fun sum(a: Int, b: Int = 0): Int = a + b

fun main() {
    print(sum(2, 2))

    print(sum(2))

    print(sum(a = 2, b = 2))
}