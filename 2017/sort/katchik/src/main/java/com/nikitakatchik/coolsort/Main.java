package com.nikitakatchik.coolsort;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.*;

public class Main {
    public static final long FILE_SIZE = 2L * 1024L * 1024L * 1024L;
    public static final int FILE_SIZE_SUB = (int) (FILE_SIZE - 2L);
    public static final int LINE_LENGTH = 76;
    public static final int LINE_SIZE = LINE_LENGTH + 1;
    public static final int LINE_COUNT = FILE_SIZE_SUB / LINE_SIZE;

    private static final int OUTPUT_BUFFER_LINES = 4 * 1024 * 1024;
    private static final int OUTPUT_BUFFER_SIZE = LINE_SIZE * OUTPUT_BUFFER_LINES;

    public static void main(final String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("java -jar katchiksort.jar input-file output-file");
            return;
        }
        final String filepath = args[0];
        final String filepathOutput = args[1];

        final ForkJoinPool pool = new ForkJoinPool();

        //try (final StopWatch stopWatchTotal = new StopWatch("Total")) {
            final Lines lines = new Lines(filepath);

            //try (final StopWatch stopWatchSort = new StopWatch("Sort")) {
                pool.invoke(new QuickSortAction(lines, 0, LINE_COUNT - 1));
            //}

            final int indexAdd = binaryInsert(lines);

            //System.out.println("Unsorted: " + testSort(lines.buffer, lines.map));

            //try (final StopWatch stopWatchSort = new StopWatch("Output")) {
                writeFile(pool, filepathOutput, lines, indexAdd);
            //}
        //}

        pool.shutdown();
    }

    public static class Lines {
        public final MappedByteBuffer buffer;
        public final int[] map = initializeMap(LINE_COUNT);

        public final byte[] add = new byte[(int) (FILE_SIZE - FILE_SIZE_SUB)];

        public Lines(final String filepath) throws IOException {
            try (final RandomAccessFile inputFile = new RandomAccessFile(filepath, "rw")) {
                inputFile.seek(FILE_SIZE_SUB);
                inputFile.read(this.add);
                inputFile.seek(0);
                try (final FileChannel fileChannel = inputFile.getChannel()) {
                    this.buffer = fileChannel.map(FileChannel.MapMode.PRIVATE, 0, FILE_SIZE_SUB);
                }
            }
        }
    }

    public static class ByteBufferFileWriter {
        private static final long NOTIFY_INTERVAL = 2 * 1024 * 1024;
        private final Object sync = new Object();
        private final byte[] buffer = new byte[OUTPUT_BUFFER_SIZE];
        private long bytesBuffered = 0;
        private long bytesNotify = NOTIFY_INTERVAL / 128;
        private long bytesOffset = 0;
        private boolean offsetRequired = false;

        public ByteBufferFileWriter(final ExecutorService executor,
                                    final Lines lines,
                                    final int indexAdd) {
            if (executor == null || lines == null) {
                throw new IllegalArgumentException();
            }

            executor.submit(() -> {
                for (int i = 0; i < LINE_COUNT + 1; i++) {
                    synchronized (this.sync) {
                        while (this.bytesBuffered - this.bytesOffset + LINE_SIZE >= this.buffer.length) {
                            this.offsetRequired = true;
                            this.sync.notify();
                            this.sync.wait();
                        }
                        this.offsetRequired = false;
                    }

                    final int written;
                    if (i != indexAdd) {
                        lines.buffer.position(lines.map[i < indexAdd ? i : i - 1] * LINE_SIZE);
                        lines.buffer.get(this.buffer, (int) (this.bytesBuffered - this.bytesOffset), LINE_SIZE);
                        written = LINE_SIZE;
                    } else {
                        System.arraycopy(lines.add, 0, this.buffer, (int) (this.bytesBuffered - this.bytesOffset), lines.add.length);
                        this.buffer[(int) (this.bytesBuffered - this.bytesOffset) + lines.add.length] = '\n';
                        written = lines.add.length + 1;
                    }

                    synchronized (this.sync) {
                        this.bytesBuffered += written;
                        if (this.bytesBuffered >= this.bytesNotify) {
                            this.bytesNotify = this.bytesBuffered + NOTIFY_INTERVAL;
                            this.sync.notify();
                        }
                    }
                }

                synchronized (this.sync) {
                    this.sync.notify();
                }
                return null;
            });
        }

        public void write(final OutputStream outputStream) throws IOException {
            long written = 0;

            while (written < FILE_SIZE) {
                final long t = System.currentTimeMillis();
                final long offset;
                final long length;
                synchronized (this.sync) {
                    while (written == this.bytesBuffered) {
                        if (this.offsetRequired) {
                            this.bytesOffset = written;
                            this.sync.notify();
                        }
                        try {
                            this.sync.wait();
                        } catch (final InterruptedException e) {
                            throw new IOException(e);
                        }
                    }

                    offset = this.bytesOffset;
                    length = this.bytesBuffered - written;
                }

                final long t1 = System.currentTimeMillis();
                outputStream.write(this.buffer, (int) (written - offset), (int) length);

                written += length;
            }
        }
    }

    public static void writeFile(final ExecutorService executor,
                                 final String filepath,
                                 final Lines lines,
                                 final int indexAdd) throws IOException, InterruptedException {
        /*
        final byte[] line = new byte[LINE_SIZE];
        try (final BufferedOutputStream outputStream  = new BufferedOutputStream(new FileOutputStream(filepath))) {
            for (int i = 0; i < LINE_COUNT; i++) {
                input.position(map[i] * LINE_SIZE);
                input.get(line);
                outputStream.write(line, 0, line.length);
            }
        }
        //*/

        try (final OutputStream outputStream  = new FileOutputStream(filepath)) {
            final ByteBufferFileWriter byteBufferFileWriter = new ByteBufferFileWriter(executor, lines, indexAdd);
            byteBufferFileWriter.write(outputStream);
        }
    }

    /*
    public static int testSort(final MappedByteBuffer input, final int[] map) {
        int warning = 0;
        for (int i = 0; i < LINE_COUNT - 1; i++) {
            if (greater(input, map[i], map[i + 1])) {
                warning++;
            }
        }
        return warning;
    }
    */

    public static int[] initializeMap(final int count) {
        final int[] map = new int[count];
        for (int i = 0; i < map.length; i++) {
            map[i] = i;
        }
        return map;
    }

    static class QuickSortAction extends RecursiveAction {
        private final Lines lines;
        private final int left;
        private final int right;

        public QuickSortAction(final Lines lines, final int left, final int right) {
            this.lines = lines;
            this.left = left;
            this.right = right;
        }

        @Override
        protected void compute() {
            final int index = partition(this.lines.buffer, this.lines.map, this.left, this.right);

            ForkJoinTask actionLeft = null;

            if (left < index - 1) {
                actionLeft = new QuickSortAction(this.lines, this.left, index - 1).fork();
            }
            if (index < right) {
                new QuickSortAction(this.lines, index, right).fork().join();
            }

            if (actionLeft != null) {
                actionLeft.join();
            }
        }
    }

    static int partition(final ByteBuffer byteBuffer, final int[] map, final int left, final int right) {
        final int pivotIndex = map[(left + right) / 2];

        int i = left;
        int j = right;

        while (i <= j) {
            while (greater(byteBuffer, pivotIndex, map[i])) {
                i++;
            }
            while (greater(byteBuffer, map[j], pivotIndex)) {
                j--;
            }
            if (i <= j) {
                swap(map, i++, j--);
            }
        }

        return i;
    }

    private static boolean greater(final ByteBuffer byteBuffer, final int indexLeft, final int indexRight) {
        int offsetLeft = indexLeft * LINE_SIZE;
        int offsetRight = indexRight * LINE_SIZE;

        int endA = offsetLeft + LINE_LENGTH;

        while (offsetLeft < endA) {
            final byte left = byteBuffer.get(offsetLeft);
            final byte right =  byteBuffer.get(offsetRight);
            if (left > right) {
                return true;
            } else if (left < right) {
                return false;
            }

            offsetLeft++;
            offsetRight++;
        }

        return false;
    }

    static void swap(final int[] array, final int a, final int b) {
        final int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }

    static int binaryInsert(final Lines lines) {
        int lowerBound = 0;
        int upperBound = LINE_COUNT - 1;
        int curIn;
        while (true) {
            curIn = (upperBound + lowerBound) / 2;
            if (greater(lines.add, lines.buffer, lines.map[curIn])) {
                lowerBound = curIn + 1; // its in the upper
                if (lowerBound > upperBound)
                    return curIn + 1;
            } else {
                upperBound = curIn - 1; // its in the lower
                if (lowerBound > upperBound)
                    return curIn;
            }
        }
    }

    /*
    public static int binarySearch(final Lines lines) {
        int lo = 0;
        int hi = LINE_COUNT - 1;
        while (lo <= hi) {
            final int mid = lo + (hi - lo) / 2;

            if (lesser(lines.add, lines.buffer, mid)) {
                hi = mid - 1;
            } else if (greater(lines.add, lines.buffer, mid)) {
                lo = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
    */


    private static boolean greater(final byte[] add, final ByteBuffer byteBuffer, final int index) {
        for (int i = 0; i < add.length; i++) {
            final byte value = byteBuffer.get(index * LINE_SIZE + i);

            if (add[i] > value) {
                return true;
            } else if (add[i] < value) {
                return false;
            }
        }

        return false;
    }

    private static boolean lesser(final byte[] add, final ByteBuffer byteBuffer, final int index) {
        for (int i = 0; i < add.length; i++) {
            final byte value = byteBuffer.get(index * LINE_SIZE + i);

            if (add[i] < value) {
                return true;
            } else if (add[i] > value) {
                return false;
            }
        }

        return true;
    }
}
