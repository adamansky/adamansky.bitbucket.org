package nsu.fit.javaperf.bc;


import java.util.ArrayList;
import java.util.List;

public class TypeCastFail {

    public static void main(String[] args) {
        List mylist = new ArrayList();
        mylist.add(new byte[][]{{1, 12}, {2, 21}, {3, 31}});
        String first = (String) mylist.get(0);
    }
}
