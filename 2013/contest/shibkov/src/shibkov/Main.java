package shibkov;

import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Alexey
 * Date: 23.11.13
 * Time: 21:53
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    private static final int NumberOfElements = 10000000;
    private static final int ChoseOfElements = 100000;

    private static final String File1 = "File1.txt";
    private static final String File2 = "File2.txt";

    public static void main(String[] args){

        long allTime = System.currentTimeMillis();

        Random random = new Random(System.currentTimeMillis());

        int[] elms = new int[NumberOfElements];
        for (int i = 0; i < NumberOfElements; ++i) {
            elms[i] = random.nextInt();
        }
        System.out.println("Created!");

        final int[] sortedElems = elms.clone();
        Arrays.sort(sortedElems);

        System.out.println("Sorted!");

        Thread t = new Thread(new Runnable() {
            public void run() {
                long cur = System.nanoTime();
                System.out.println("Start writing");
                Writer writer = null;
                try {
                    writer = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(File1), "utf-8"));
                    for (int elem : sortedElems) {
                        writer.write(elem + "\n");
                    }
                } catch (IOException ex) {
                } finally {
                    try {writer.close();} catch (Exception ex) {}
                }
                System.out.println("Finished writing! " + (System.nanoTime() - cur));
            }
        });
        t.start();

        long cur = System.nanoTime();
        System.out.println("Start search");
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(File2), "utf-8"));
            Map<Integer, Integer> map = new HashMap<Integer, Integer>(ChoseOfElements);

            int n;
            for (int elem : elms) {
                if (!map.containsKey(elem)) {
                    n = getNumberOfElements(sortedElems, elem);
                    map.put(elem, n);
                    writer.write(elem + " " + n + "\n");
                    if (map.size() == ChoseOfElements) {
                        break;
                    }
                }
            }

        } catch (IOException e) {
        } finally {
            try {writer.close();} catch (Exception ex) {}
        }

        System.out.println("Finished search! " + (System.nanoTime() - cur));

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        System.out.println("T=" + (System.currentTimeMillis() - allTime));
    }

    static int getNumberOfElements(int[] sortedElems, int f) {
        int index = Arrays.binarySearch(sortedElems, f);
        while (true)
        {
            if (index == 0) {
                break;
            }
            if (sortedElems[index] == sortedElems[index - 1]) {
                index--;
            } else {
                break;
            }
        }

        int numbers = 0;
        for (int i = index; i < NumberOfElements; ++i) {
            if (sortedElems[i] == f) {
                numbers++;
            } else {
                break;
            }
        }

        return numbers;
    }

}