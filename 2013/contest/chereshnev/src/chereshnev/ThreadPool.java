package chereshnev;

import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

class ThreadPool extends ThreadPoolExecutor
{
	private AtomicLong queueCount = new AtomicLong(0);
	public ThreadPool(int nThreads)
	{
		super(nThreads, nThreads, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}
	@Override
	public void execute(final Runnable runnable)
	{
		assert runnable != null;
		queueCount.incrementAndGet();
		super.execute(new Runnable()
		{
			public void run()
			{
				runnable.run();
				if (queueCount.decrementAndGet() == 0)
					shutdown();
			}
		});
	}
	public void waitDone()
	{
		try
		{
			awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
		}
		catch (InterruptedException e)
		{
			System.err.println("Thread was interrupted while waiting thread pool termination");
		}
	}
}