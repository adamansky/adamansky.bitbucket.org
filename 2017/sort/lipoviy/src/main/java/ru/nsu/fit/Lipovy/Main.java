package ru.nsu.fit.Lipovy;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Need to specify the path to the file");
            return;
        }

        FileSorter sorter = new FileSorter(args[0], 10);
        long start = System.currentTimeMillis();
        sorter.start();
        System.out.println("\tTotal run time: \u001B[31;1m" + (System.currentTimeMillis() - start) + " ms\u001B[0m");

        return;
    }
}
