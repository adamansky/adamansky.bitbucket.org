package limorenko;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Main
{
    public static void main(String[] args)
    {
        long start = System.currentTimeMillis();
        ArrayList<Integer> myList = new ArrayList<Integer>();
        Random intgenerator = new Random();

        for(long i = 0; i < 10000000; i++)
            myList.add(intgenerator.nextInt());
        Collections.sort(myList);
        try
        {
            PrintWriter printWriter = new PrintWriter("tmp.txt");
            for (Integer i: myList)
            {
                printWriter.println(i);
            }
            printWriter.close ();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("FileNotFound writing into tmp.txt\n");
        }
        myList.clear();


        HashMap<Integer, Integer> map1o1= new HashMap<Integer, Integer>();
        try
        {
            Scanner scanner = new Scanner(new File("tmp.txt"));
            int value = scanner.nextInt();
            int count = 1;
            while(scanner.hasNextInt())
            {
                int newvalue = scanner.nextInt();
                if(value == newvalue)
                    count++;
                else
                {
                    map1o1.put(value,count);
                    count = 1;
                    value = newvalue;
                }
            }
            scanner.close();
            map1o1.put(value,count);
        }
        catch(FileNotFoundException e)
        {
            System.out.println("FileNotFound reading from tmp.txt\n");
        }

        try
        {
            PrintWriter printWriter = new PrintWriter("result.txt");
            for(long i = 0; i < 100000; i++)
            {
                int value = intgenerator.nextInt();
                if(map1o1.get(value)!=null)
                {
                    printWriter.println(value + " " + map1o1.get(value));
                    System.out.println("1\n");
                }
                else
                    printWriter.println(value + " " + 0);
            }
            printWriter.close ();
        }
        catch(FileNotFoundException e)
        {
            System.out.println("FileNotFound writing into result.txt\n");
        }
        map1o1.clear();

        long end = System.currentTimeMillis();
        System.out.println("T=" + ((end - start)));
    }
}