package com.nikitakatchik.coolsort;

import java.util.Random;

public class QuickSort {
    static void quickSort(final int[] arr, final int[] map, final int left, final int right) {
        final int index = partition(arr, map, left, right);
        if (left < index - 1) {
            quickSort(arr, map, left, index - 1);
        }
        if (index < right) {
            quickSort(arr, map, index, right);
        }
    }

    static boolean greater(final int[] arr, final int indexLeft, final int indexRight) {
        return arr[indexLeft] > arr[indexRight];
    }

    static int partition(final int[] arr, final int[] map, final int left, final int right) {
        final int pivotIndex = map[(left + right) / 2];

        int i = left;
        int j = right;

        while (i <= j) {
            while (greater(arr, pivotIndex, map[i])) {
                i++;
            }
            while (greater(arr, map[j], pivotIndex)) {
                j--;
            }
            if (i <= j) {
                swap(map, i++, j--);
            }
        }

        return i;
    }

    static void swap(final int[] array, final int a, final int b) {
        final int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }

    public static void main(String[] args) {
        final Random random = new Random();
        final int[] arr = new int[1024*1024];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Math.abs(random.nextInt());
        }

        final int[] map = Main.initializeMap(arr.length);
        quickSort(arr, map, 0, arr.length - 1);

        int warning = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[map[i]] > arr[map[i + 1]]) {
                warning++;
            }
        }
        System.out.println("Bads: " + warning);
    }
}
