package nsu.fit.javaperf.lab2;
public class P1 {
    public static void main(String[] args) {
        long ms = System.currentTimeMillis();
        String s = "";
        for (int i = 0; i < 10000; ++i) {
            s += " " + String.valueOf(i);
        }
        System.out.println(s.length());
        System.out.println("Time: " + (System.currentTimeMillis() - ms));
    }
}
