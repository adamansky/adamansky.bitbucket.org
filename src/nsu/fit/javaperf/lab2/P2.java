package nsu.fit.javaperf.lab2;
public class P2 {
    public static void main(String[] args) {
        long ms = System.currentTimeMillis();
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 10000; ++i) {
            s.append(" ").append(String.valueOf(i));
        }
        System.out.println(s.length());
        System.out.println("Time " + (System.currentTimeMillis() - ms));
    }
}
