package ru.nsu.opj;

import java.util.concurrent.atomic.AtomicInteger;

public class Lambdas {

    public interface Supplier { // Single Abstract Method  (SAM)
        String get();
    }

    public static void greeting(Supplier smb) { //
        System.out.println(smb.get());
    }

    public static void main(String[] args) {

        // int i = 0;
        AtomicInteger iholder = new AtomicInteger(0);

        greeting(() -> "Hello!!!");

        greeting(() -> {       // 1.8
            iholder.addAndGet(1);
            return "Hello!!! " + iholder.get();
        });


        greeting("Andy"::toString);

    }
}
