package com.nikitakatchik.coolsort;

public class StopWatch implements AutoCloseable {
    private final long time = System.currentTimeMillis();
    private final String tag;

    public StopWatch(final String tag) {
        this.tag = tag;
    }

    @Override
    public void close() {
        final long time = System.currentTimeMillis() - this.time;
        System.out.println(String.format("Time (%s): %dms", this.tag, time));
    }
}
