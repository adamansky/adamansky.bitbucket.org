import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mikhail on 07/12/2017, 15:46;
 */

public class Base64Sort {

	private static AtomicInteger mode;

	private static RandomAccessFile file;

	private static int linesTables[][];

	private static int linesTablesCount[];

	private static int segment;

	private static byte stringBuffers[][];

	private static final Object readMonitor = new Object();

	private static final Object waitMonitor = new Object();

	private static final LinkedBlockingQueue<byte[]> writeQueue = new LinkedBlockingQueue<>(1048576);

	public static void main(String args[]) {
		if (args.length < 1) {
			System.out.println("Usage: java -jar base64sort.jar <base64 encoded file>\n\tCreates \"out.data\" file with sorted strings.");
			return;
		}

		File inputFile = new File(args[0]);
		try {
			file = new RandomAccessFile(inputFile, "r");
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			return;
		}

		mode = new AtomicInteger(0);
		segment = 0;
		linesTablesCount = new int[27];
		linesTables = new int[27][]; /* 2^31 / (77 * 2^20) */
		for (int i = 0; i < 26; ++i) {
			linesTables[i] = new int[1048576]; /* 2^20 */
			linesTables[i][0] = i;
		}
		linesTables[26] = new int[626423]; /* (2^31 / 77) - (26 * 2^20) */
		linesTables[26][0] = 26;
		stringBuffers = new byte[27][77];

		for (int i = 0; i < 7; ++i) {
			new Thread(Base64Sort::threadBody).start();
		}
		threadBody();
	}

	private static void threadBody() {
		byte buffer[] = new byte[80740352]; /* 77 Mb */
		int table[];
		byte stringBuffer[];

		try {
			while ((table = readNextSegment(buffer)) != null) {
				int segment = table[0];
				for (int i = 0; i < table.length; ++i) {
					table[i] = i * 77;
				}
				heapSort(table, buffer);

				stringBuffer = stringBuffers[segment];
				int offset = table[0];
				System.arraycopy(buffer, offset, stringBuffer, 0, 77);
				if (stringBuffer[76] != '\n') {
					stringBuffer = Arrays.copyOfRange(stringBuffer, 0, 3);
					stringBuffer[2] = '\n';
				}
				++linesTablesCount[segment];

				int segmentOffset = segment * 1048576 * 77;
				for (int i = 0; i < table.length; ++i) {
					table[i] += segmentOffset;
				}
			}
		} catch (IOException e) {
			System.err.println("Reading error");
			System.exit(1);
		}
		buffer = null;

		int m = mode.getAndIncrement();
		if (m == 0) {
			waitForOthers();
			mergeSegments();
		} else if (m == 1) {
			waitForOthers();
			writeSegments();
		} else if (m == 7) {
			synchronized (waitMonitor) {
				waitMonitor.notifyAll();
			}
		}
	}

	private static void waitForOthers() {
		synchronized (waitMonitor) {
			if (mode.get() == 8) {
				return;
			}
			for (;;) {
				try {
					waitMonitor.wait();
					if (mode.get() == 8) {
						return;
					}
				} catch (InterruptedException e) {
					continue;
				}
			}
		}
	}

	private static void mergeSegments() {
		int tablesCount[] = linesTablesCount;
		int tables[][] = linesTables;
		int table[] = new int[27];
		for (int i = 0; i < 27; ++i) {
			table[i] = i;
		}
		byte buffers[][] = stringBuffers;
		int buffersSize = buffers.length;
		sort(table, buffers);

		for (;;) {
			while (!writeQueue.offer(buffers[table[0]]));

			buffers[table[0]] = new byte[77];
			int segment = table[0];
			int count = tablesCount[segment]++;
			if (count >= tables[segment].length) {
				if (--buffersSize == 0) {
					return;
				}
				shiftDown(table);
			} else {
				int index = tables[segment][count];
				int size;
				try {
					size = readRandomString(buffers[table[0]], index);
				} catch (IOException e) {
					System.err.println("Reading error");
					System.exit(1);
					return;
				}

				if (size < 77) {
					buffers[table[0]] = Arrays.copyOfRange(buffers[table[0]], 0, size + 1);
					buffers[table[0]][size] = '\n';
				}
				insert(table, buffers, buffersSize);
			}
		}
	}

	private static void sort(int table[], byte buffers[][]) {
		for (int i = 0; i < buffers.length - 1; ++i) {
			for (int j = i + 1; j < buffers.length; ++j) {
				if (compareStrings(buffers[table[i]], buffers[table[j]]) > 0) {
					swap(table, i, j);
				}
			}
		}
	}

	private static void shiftDown(int table[]) {
		System.arraycopy(table, 1, table, 0, table.length - 1);
		table[table.length - 1] = -1;
	}

	private static void insert(int table[], byte buffers[][], int size) {
		for (int i = 1; i < size; ++i) {
			if (compareStrings(buffers[table[i - 1]], buffers[table[i]]) > 0) {
				swap(table, i - 1, i);
			} else {
				return;
			}
		}
	}

	private static void writeSegments() {
		File outputFile = new File("out.data");
		FileOutputStream output;
		try {
			output = new FileOutputStream(outputFile);
		} catch (FileNotFoundException e) {
			System.err.println("Error: can not create output file");
			System.exit(1);
			return;
		}

		ByteBuffer writeBuffer = ByteBuffer.allocate(80740352); /* 77 Mb */
		byte buffer[];
		int count = 0;
		for (;;) {
			try {
				if (count == 27889399) {
					output.write(writeBuffer.array(), 0, writeBuffer.position() - 1);
					break;
				}

				buffer = writeQueue.poll();
				if (buffer == null) {
					continue;
				}
				++count;

				writeBuffer.put(buffer);
				if (writeBuffer.remaining() < 77) {
					output.write(writeBuffer.array(), 0, writeBuffer.position());
					writeBuffer.position(0);
				}
			} catch (IOException ie) {
				System.err.println("Write error");
				System.exit(1);
				return;
			}
		}
		System.out.println("End of copying");

		try {
			output.flush();
			output.close();
		} catch (IOException e) {
			System.err.println("Write error");
			System.exit(1);
		}
	}

	private static void heapSort(int table[], byte buffer[]) {
		/* build heap */
		for (int i = table.length / 2; i >= 0; --i) {
			siftDown(table, buffer, i, table.length);
		}

		/* sort heap */
		int h = table.length;
		for (int i = 0; i < table.length; ++i) {
			swap(table, 0, h - 1);
			--h;
			siftDown(table, buffer, 0, h);
		}
	}

	private static void siftDown(int table[], byte buffer[], int i, int heapSize) {
		int j = i;
		while ((2 * j + 1) < heapSize) {
			int l = 2 * j + 1;
			int r = 2 * j + 2;
			int k = l;
			if ((r < heapSize) && (compareStrings(buffer, table[r], table[l]) >= 0)) {
				k = r;
			}
			if (compareStrings(buffer, table[j], table[k]) > 0) {
				break;
			}
			swap(table, j, k);
			j = k;
		}
	}

	private static void swap(int table[], int x, int y) {
		int t = table[x];
		table[x] = table[y];
		table[y] = t;
	}

	private static int compareStrings(byte buffer1[], byte buffer2[]) {
		int i = 0;
		byte b1;
		byte b2;
		do {
			b1 = buffer1[i];
			b2 = buffer2[i];
			++i;
		} while (i < 76 && b1 == b2);
		return b1 - b2;
	}


	private static int compareStrings(byte buffer[], int o1, int o2) {
		int i = 0;
		byte b1;
		byte b2;
		do {
			b1 = buffer[o1 + i];
			b2 = buffer[o2 + i];
			++i;
		} while (i < 76 && b1 == b2);
		return b1 - b2;
	}

	private static int[] readNextSegment(byte buffer[]) throws IOException {
		synchronized (readMonitor) {
			if (segment > 26) {
				return null;
			}
			file.read(buffer);
			if (segment == 26) {
				Arrays.fill(buffer, 48234496, 48234576, (byte)0); /* last line offset */
			}
			return linesTables[segment++];
		}
	}

	private static int readRandomString(byte buffer[], int offset) throws IOException {
		file.seek(offset);
		return file.read(buffer);
	}

}
