package pro.batalin.op.sort2;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private final static int lineLength = 77;
    private final static int threadCount = 7;
    private final static int maxBufferSizePerThread = 13 * 1024 * 1024;
    private ExecutorService threadPool = Executors.newFixedThreadPool(threadCount);
    private final ConcurrentLinkedQueue<Data> sortedData = new ConcurrentLinkedQueue<>();
    private final Map<Integer, Data> toWrite = new HashMap<>();

    class Sorter implements Runnable {
        private Data data;
        private byte[] array;

        public Sorter(Data data) {
            this.data = data;
            this.array = data.buffer.array();
        }

        @Override
        public void run() {
            data.buffer.limit((int) data.size);

            qsort(0, (int) (data.size - lineLength));

            data.buffer.flip();
            sortedData.add(data);
            data = null;
            array = null;
        }

        void qsort(int begin, int end) {
            if (end > begin) {
                int index = partition(begin, end);
                qsort(begin, index - lineLength);
                qsort(index + lineLength,  end);
            }
        }

        int getMiddle(int begin, int end) {
            begin /= lineLength;
            end /= lineLength;
            int index = begin + (end - begin) / 2;
            return index * lineLength;
        }

        private int partition(int begin, int end) {
            int index = getMiddle(begin, end);

            swap(index, end);
            for (int i = index = begin; i < end; i += lineLength) {
                if (cmp(i, end) <= 0) {
                    swap(index, i);
                    index += lineLength;
                }
            }
            swap(index, end);
            return (index);
        }

        int cmp(int a, int b) {
            if (a == b) {
                return 0;
            }

            for (int i = 0; i < lineLength; ++i) {
                if (array[a + i] == array[b + i]) {
                    continue;
                }

                return array[a + i] - array[b + i];
            }

            return 0;
        }

        void swap(int a, int b) {
            for (int i = 0; i < lineLength; ++i) {
                byte tmp = array[a + i];
                array[a + i] = array[b + i];
                array[b + i] = tmp;
            }
        }
    }

    class Data {
        public ByteBuffer buffer;
        public long size;
        public FileChannel channel;

        public Data(ByteBuffer buffer, long size) {
            this.buffer = buffer;
            this.size = size;
        }
    }

    class Node {
        public ByteBuffer buffer;
        public FileChannel channel;
    }

    public void run(String filename) throws Exception {
        RandomAccessFile file = new RandomAccessFile(filename, "r");
        FileChannel channel = file.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(maxBufferSizePerThread);

        List<Integer> toDel = new ArrayList<>(30);
        int pushed = 0;
        int index = 0;
        int size = 0; boolean hasInput = true;
        do {

            // читаем
            if (hasInput) {
                size = channel.read(buffer);
                if (size == -1) {
                    hasInput = false;
                }
            }

            if ((hasInput || buffer.position() != 0) && sortedData.size() + toWrite.size() < threadCount) {
                int currentBufferSize = buffer.position();
                int rest = hasInput || currentBufferSize > lineLength ? currentBufferSize % lineLength : 0;
                int usefulSize = currentBufferSize - rest;

                if (usefulSize > 0) {
                    if (hasInput) {
                        ByteBuffer newBuffer = ByteBuffer.allocate(maxBufferSizePerThread);
                        newBuffer.put(buffer.array(), usefulSize, rest);

                        threadPool.execute(new Sorter(new Data(buffer, usefulSize)));
                        buffer = newBuffer;
                    } else {
                        buffer.flip();
                        sortedData.add(new Data(buffer, usefulSize));

                        buffer = ByteBuffer.allocate(1);
                    }
                    ++pushed;
                }
            }

            // очередь на запись
            while (!sortedData.isEmpty()) {
                Data data = sortedData.poll();
                data.channel = new FileOutputStream(filename + "." + index, false).getChannel();
                toWrite.put(index, data);
                ++index;
            }

            // запись
            for (Map.Entry<Integer, Data> entry : toWrite.entrySet()) {
                Data data = entry.getValue();
                data.channel.write(data.buffer);
                if (!data.buffer.hasRemaining()) {
                    data.channel.close();
                    toDel.add(entry.getKey());
                }
            }

            // удаление записанных
            toDel.forEach(toWrite::remove);

        } while (hasInput || buffer.position() != 0 || !sortedData.isEmpty() || !toWrite.isEmpty() || index < pushed);

        channel.close();

        threadPool.shutdown();
        threadPool = null;
        file = null;
        channel = null;
        buffer = null;

        // merge
        merge2(index, filename);
    }

    class Node2 {
        public String str;
        public BufferedReader reader;
    }

    void merge2(int count, String filename) throws Exception {
        PriorityQueue<Node2> heap = new PriorityQueue<>(count, (a, b) -> a.str.compareTo(b.str));
        for (int i = 0; i < count; ++i) {
            Node2 node = new Node2();
            File part = new File(filename + "." + i);
            node.reader = Files.newBufferedReader(part.toPath());
            node.str = node.reader.readLine();

            if (node.str != null) {
                heap.add(node);
            }
        }

        File result = new File(filename + ".res");

        try (BufferedWriter writer = Files.newBufferedWriter(result.toPath(),
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)) {

            while (!heap.isEmpty()) {
                Node2 node = heap.poll();
                writer.write(node.str);
                writer.newLine();

                node.str = node.reader.readLine();
                if (node.str != null) {
                    heap.add(node);
                }
            }
        }
    }

    void merge(int index, String filename, List<Integer> toDel) throws Exception {
        PriorityQueue<Node> heap = new PriorityQueue<>(index, (a, b) -> {
            if (a == b) {
                return 0;
            }
            return cmp(a.buffer, b.buffer);
        });
        Map<Integer, Node> queue = new HashMap<>(index);
        int mergeMaxSize = maxBufferSizePerThread * threadCount / index;
        for (int i = 0; i < index; ++i) {
            Node node = new Node();
            node.channel = new FileInputStream(filename + "." + i).getChannel();
            node.buffer = ByteBuffer.allocate(mergeMaxSize);
            int count = node.channel.read(node.buffer);

            if (count == -1) {
                continue;
            }

            if (count < lineLength) {
                queue.put(i, node);
            } else {
                heap.add(node);
            }
        }

        while (!queue.isEmpty()) {
            toDel.clear();
            for (Map.Entry<Integer, Node> node : queue.entrySet()) {
                int code = node.getValue().channel.read(node.getValue().buffer);
                int pos = node.getValue().buffer.position();

                if (code == -1 && pos == 0) {
                    toDel.add(node.getKey());
                    continue;
                }

                if (pos >= lineLength) {
                    toDel.add(node.getKey());
                    heap.add(node.getValue());
                }

                if (code == -1 && pos > 0) {
                    toDel.add(node.getKey());
                    heap.add(node.getValue());
                }
            }
            toDel.forEach(queue::remove);
        }


        try (FileChannel output = new FileOutputStream(filename + ".res", false).getChannel()) {

            while (!heap.isEmpty()) {
                Node node = heap.poll();

                node.buffer.flip();
                int limit = node.buffer.limit();
                node.buffer.limit(lineLength);
                output.write(node.buffer);
                node.buffer.limit(limit - lineLength);

                node.buffer.compact();

                int code = node.channel.read(node.buffer);
                while (node.buffer.position() < lineLength && code != -1) {
                    code = node.channel.read(node.buffer);
                }

                if (node.buffer.position() >= lineLength) {
                    heap.add(node);
                    continue;
                }
                if (node.buffer.position() > 0) {
                    heap.add(node);
                }
            }
        }
    }

    public void sout(ByteBuffer byteBuffer) {
        String v = new String( byteBuffer.array(), Charset.forName("UTF-8") );
        System.out.println("\"" + v + "\"");
    }

    int cmp(ByteBuffer a, ByteBuffer b) {
        int limA = a.limit();
        int limB = b.limit();
        int count = Math.min(lineLength, Math.min(limA, limB));

        byte[] arrA = a.array();
        byte[] arrB = b.array();
        for (int i = 0; i < count; ++i) {
            if (arrA[i] == arrB[i]) {
                continue;
            }

            return arrA[i] - arrB[i];
        }

        if (count == lineLength) {
            return 0;
        }

        return limA - limB;
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Need filename");
            return;
        }

        long start = System.currentTimeMillis();
        new Main().run(args[0]);
        long end = System.currentTimeMillis();

        System.out.println("Total: " + ((end - start) / 1000));
        return;
    }
}
