package ru.nsu

fun start(arg: String?) {
    println(arg?.length)
}

fun start2(arg: String?) {
    println(arg?.length ?: 0)
}

fun main(args: Array<String>) {
    start(null)
    start2("test")
}