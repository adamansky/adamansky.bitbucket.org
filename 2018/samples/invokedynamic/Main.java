
import java.util.function.Consumer;

public class Main {

    static void hello(Consumer<String> cb) {
        cb.accept("Hello");
    }

    public static void main(String[] args) {
        hello(s -> {
            System.out.println(s);
        });
    }
}
