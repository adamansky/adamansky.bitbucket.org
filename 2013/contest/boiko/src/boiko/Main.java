package boiko;//import java.util.Random;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static int[] firstPartOfNumbers = new int[100000];
    public static int[] randomNumbers = new int[10000000];

    private static void fillArrayClaiisc() {
        ThreadLocalRandom generator = ThreadLocalRandom.current();
        int randomNumber = 0;
        for (int i = 0; i < 10000000; i++) {
            randomNumber = generator.nextInt();
            randomNumbers[i] = randomNumber;
        }
        // Arrays.sort(randomNumbers);
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        ParallelFillArray pFillArray = new ParallelFillArray();
        pFillArray.start();
        Arrays.sort(firstPartOfNumbers);
        Arrays.sort(randomNumbers);
        try {
            RandomAccessFile memoryMappedFile = new RandomAccessFile("output",
                                                                     "rw");
            MappedByteBuffer out = memoryMappedFile.getChannel().map(
                    FileChannel.MapMode.READ_WRITE, 0, 10000000 * 4);
            for (int i = 0; i < 10000000; i++) {
                out.putInt(randomNumbers[i]);
            }
            out.load();
            // End of first part of task.
            // Start of second: load file (out.asIntBuffer()) and create a table using binary search.
            CreateTable(randomNumbers);
        } catch (Exception e) {
            // ignore
            e.printStackTrace();
        }
        System.out.println("T=" + (System.currentTimeMillis() - start));
    }

    private static void CreateTable(int[] arrayOnWhichSearch) {
        RandomAccessFile memoryMappedFile;
        try {
            memoryMappedFile = new RandomAccessFile("table",
                                                    "rw");
            MappedByteBuffer out = memoryMappedFile.getChannel().map(
                    FileChannel.MapMode.READ_WRITE, 0, 100000 * 12);
            for (int i = 0; i < 100000; i++) {
                out.putInt(firstPartOfNumbers[i]);
                out.putChar(' ');
                out.putInt(BinarySearch(arrayOnWhichSearch, firstPartOfNumbers[i]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // I've thought that it's quite not smart to perform binary search on bounds [INT_MIN:INT_MAX]
    // Considering that data has linear distribution, we can guess position of data, or at least
    // most probable bounds in which searched value is placed. Guessed position is presented
    // by variable "position", guessed bounds by variables "left" and "right".
    private static int BinarySearch(int[] arrayOnWhichToSearch, int number) {
        int position = (number / 430) + 4994148;
        int offset = 10;
        int left = Math.max(position - offset, 0);
        int right = Math.min(position + offset, 9999999);
        if (arrayOnWhichToSearch[position] == number) {
            return BinarySeaarchFound(arrayOnWhichToSearch, number, position);
        }
        while (arrayOnWhichToSearch[left] > number || arrayOnWhichToSearch[right] < number) {
            offset *= 4;
            left = Math.max(position - offset, 0);
            right = Math.min(position + offset, 9999999);
        }
        int peek;
        while (right > left + 1) {
            peek = (left + right) / 2;
            if (arrayOnWhichToSearch[peek] == number) {
                return BinarySeaarchFound(arrayOnWhichToSearch, number, peek);
            }
            left = Math.max(arrayOnWhichToSearch[peek] < number ? peek : left, 0);
            right = Math.min(arrayOnWhichToSearch[peek] > number ? peek : right, 9999999);
        }
        return BinarySeaarchFound(arrayOnWhichToSearch, number, left);
    }

    // Returns number of occurences of "number" in sequence "arrayOnWhichToSearch" near "position"
    // as data is ordered, this number is also number of occurences in all the sequence.
    private static int BinarySeaarchFound(int[] arrayOnWhichToSearch, int number, int position) {
        int result = arrayOnWhichToSearch[position] == number ? 1 : 0;
        int j = 1;
        while (arrayOnWhichToSearch[position + j] == number) {
            result++;
            j++;
        }
        j = -1;
        while (arrayOnWhichToSearch[position + j] == number) {
            result++;
            j--;
        }
        return result;
    }

}

// Fills array with random numbers -- there are only 10kk random numbers generated, by "ThreadLocalRandom".
// Idea is based that the bottleneck of program is in Arrays.sort() method, on my machine it takes 1 sec to sort
// an array of random values places there by function "fillArrayClaiisc" presented above (not used in the program).
// Such long time is taken because on random data Timsort (used in java 1.7, on which I've tested (java 1.6 and 
// previous used quicksort)), gives O(Nlog(N)) time. But if data is partly ordered it takes only O(n) to sort the array (https://habrahabr.ru/post/133996/)
// So, I've decided to fill array partly ordering the data. To fill array in such a way takes me 0.5 seconds, comparing to
// 0.2 seconds of "fillArrayClassic", but the Arrays.sort() on this data gives me time of 0.5 seconds comparing to
// 1 second of classically filled arary. So, on my machine I've got win in time in 0.2 seconds here what is quite good,
// because whole program on my machine (including creation of table and writing to file) 1.2 seconds (cometimes even faster).
class ParallelFillArray {
    private static final int THREADS = Runtime.getRuntime()
                                               .availableProcessors() * 2;
    private static final ExecutorService THREADS_POOL = Executors
            .newFixedThreadPool(THREADS);

    void start() {
        int rangePerThread = 10000000 / THREADS;
        int valueRangePerThread = (int) (((long) Integer.MAX_VALUE - (long) Integer.MIN_VALUE) / THREADS);
        ArrayList<Future<?>> futureList = new ArrayList<Future<?>>();
        for (int i = 0; i < THREADS; i++) {
            Runnable r = new FillArray(Integer.MIN_VALUE + i
                                                           * valueRangePerThread, Integer.MIN_VALUE + (i + 1)
                                                                                                      * valueRangePerThread, i * rangePerThread, (i + 1)
                                                                                                                                                 * rangePerThread);
            futureList.add(THREADS_POOL.submit(r));
        }
        // I'm not very common with "Future" class, but it happens, that somehow
        // these objects are sometimes
        // modified outside the cycle, so I put the cycle in synchronized block.
        synchronized (this) {
            for (Future<?> f : futureList) {
                try {
                    f.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
        THREADS_POOL.shutdownNow();
    }

    class FillArray implements Runnable {
        private int m_start;
        private int m_end;
        private int m_rangeStart;
        private int m_rangeEnd;
        private int m_smallRangeStart;
        private int m_smallRangeEnd;

        FillArray(int start, int end, int rangeStart, int rangeEnd) {
            m_start = start + 1;
            m_end = end - 1;
            m_rangeStart = rangeStart;
            m_rangeEnd = rangeEnd;
            m_smallRangeStart = rangeStart / 100;
            m_smallRangeEnd = rangeEnd  / 100;
        }

        public void run() {
            // works two times faster than java.util.Random
            ThreadLocalRandom generator = ThreadLocalRandom.current();
            int randomNumber = 0;
            int position = 0;
            int j = 0;
            int k = m_rangeStart;
            int limit = 2500;
            for (int i = m_rangeStart; i < m_rangeEnd; i++) {
                randomNumber = generator.nextInt(m_start, m_end);
                if (i - m_rangeStart < m_smallRangeEnd - m_smallRangeStart) {
                    Main.firstPartOfNumbers[m_smallRangeStart + i - m_rangeStart] = randomNumber;
                }
                if (randomNumber == 0) {
                    continue;
                }
                position = (randomNumber / 430) + 4994148;
                if (Main.randomNumbers[position] == 0) {
                    Main.randomNumbers[position] = randomNumber;
                } else {
                    if (Main.randomNumbers[position] < randomNumber) {
                        for (j = position + 1; j < Math.min(position + limit,
                                                            m_rangeEnd); j++) {
                            if (Main.randomNumbers[j] == 0) {
                                Main.randomNumbers[j] = randomNumber;
                                break;
                            }
                        }
                        if (j == Math.min(position + limit, m_rangeEnd)) {
                            for (; k < m_rangeEnd; k++) {
                                if (Main.randomNumbers[k] == 0) {
                                    Main.randomNumbers[k] = randomNumber;
                                    break;
                                }
                            }
                        }
                    } else {
                        for (j = position - 1; j < Math.max(position - limit,
                                                            m_rangeStart); j--) {
                            if (Main.randomNumbers[j] == 0) {
                                Main.randomNumbers[j] = randomNumber;
                                break;
                            }
                        }
                        if (j == Math.max(position - limit, m_rangeStart)) {
                            for (; k < m_rangeEnd; k++) {
                                if (Main.randomNumbers[k] == 0) {
                                    Main.randomNumbers[k] = randomNumber;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}