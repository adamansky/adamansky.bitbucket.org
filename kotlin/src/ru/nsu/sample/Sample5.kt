package ru.nsu.sample

fun main() {
    val name = "Andy"
    val ret = if (name == "Andy") "yes" else "no"
    print("$ret")


    val result = try {
        readLine()
    } catch (e: Exception) {
        "Oops..."
    }
    print("Result: ${result}")
}
