package ru.nsu.fit.fedin;

import java.io.*;

public class BinaryFileBuffer  {
    public BufferedReader br;
    public File sourceFile;
    private String next;
    private boolean empty;

    public BinaryFileBuffer(File f) throws IOException {
        sourceFile = f;
        br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
        reload();
    }

    public boolean empty() {
        return empty;
    }

    private void reload() throws IOException {
        try {
            if((this.next = br.readLine()) == null){
                empty = true;
                next = null;
            }
            else{
                empty = false;
            }
        } catch(EOFException oef) {
            empty = true;
            next = null;
        }
    }

    public void close() throws IOException {
        br.close();
    }


    public String peek() {
        if(empty()) return null;
        return next;
    }

    public String pop() throws IOException {
        String answer = peek();
        reload();
        return answer;
    }



}