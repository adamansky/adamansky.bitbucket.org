import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Main {
    public static String FILE_NAME = "./test.data";
    public static int LINE_LENGTH = 78;
    public static int BLOCK_SIZE = 400000;
    public static long THREADS_NUM = 8;

    public static AtomicLong maxFileNum = new AtomicLong();

    public static void main(String[] args) {
	if (args.length > 0) {
	    FILE_NAME = args[0];
	}

        List<FileReaderThread> threads = new LinkedList<>();

        for (int i = 0; i < THREADS_NUM; i++) {
            FileReaderThread thread = new FileReaderThread(i);
            thread.start();
            threads.add(thread);
        }


        threads.forEach(x -> {
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        long maxFile = maxFileNum.get();
//        Node[] nodes = new Node[maxFile + 1];
        PriorityQueue<Node> nodesQueue = new PriorityQueue<>((int)maxFile + 1, Comparator.comparing(x -> x.str));

        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("sorted.data")))) {
            for (int i = 0; i <= maxFile; i++) {
                RandomAccessFile raf = new RandomAccessFile("file_" + i, "r");
                nodesQueue.add(new Node(raf.readLine(), raf));
            }
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("sorted.data")));

            while (nodesQueue.size() != 0) {
                Node n = nodesQueue.poll();
                writer.write(n.str + "\r\n");
                String newStr = n.raf.readLine();
                n.counter++;

                if (newStr == null) {
                    System.out.println(n.counter);
                    continue;
                }

                if (newStr.length() != 0) {
                    n.str = newStr;
                    nodesQueue.add(n);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static class Node {
        public String str;
        public RandomAccessFile raf;

        public Node(String str, RandomAccessFile raf) {
            this.str = str;
            this.raf = raf;
        }

        public int counter = 0;
    }


    public static class FileReaderThread extends Thread {

        private long blockNumber;

        public FileReaderThread(int blockNumber) {
            this.blockNumber = blockNumber;
        }

        @Override
        public void run() {
            try {
                RandomAccessFile raf = new RandomAccessFile(FILE_NAME, "r");

                long index = 0;
                while (true) {
                    long blockN = blockNumber + index * THREADS_NUM;
                    raf.seek(blockN * BLOCK_SIZE * LINE_LENGTH);

                    String[] lines = new String[BLOCK_SIZE];

                    int size = BLOCK_SIZE;

                    for (int i = 0; i < BLOCK_SIZE; i++) {
                        String s = raf.readLine();
                        if (s == null) {
                            size = i;
                            break;
                        }
                        lines[i] = s;
                    }

                    if (size == 0) {
                        break;
                    }

                    Arrays.sort(lines, 0, size);


                    try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("./file_" + blockN)))) {
                        for (int i = 0; i < size; i++) {
                            writer.write(lines[i] + "\r\n");
                        }
                    }


                    ++index;
                    if (size != BLOCK_SIZE) {
                        maxFileNum.getAndUpdate(x -> Math.max(blockN, x));
                        break;
                    }
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
