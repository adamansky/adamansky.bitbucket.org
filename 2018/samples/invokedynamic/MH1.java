import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;

/**
 * @author Adamansky Anton (adamansky@softmotions.com)
 */

class Base {

    String hello(String name) {
        return "Hello0" + name;
    }
}

public class MH1 extends Base {

    @Override
    String hello(String name) {
        return "Hello " + name;
    }

    static String hello2(String name) {
        return "Hello " + name;
    }

    public static void main(String[] args) throws Throwable {

        MethodType helloMtype = MethodType.methodType(String.class, String.class);
        MethodHandle mh = MethodHandles.lookup().findVirtual(MH1.class, "hello", helloMtype);
        MH1 obj = new MH1();

        // ---- Reflections


        Method rm = MH1.class.getDeclaredMethod("hello", String.class);
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000_000_00; ++i) {
            rm.invoke(obj, "Andy");
        }
        System.out.println("Reflections: " + (System.currentTimeMillis() - start) + " ms");


        start = System.currentTimeMillis();
        for (int i = 0; i < 1000_000_00; ++i) {
            mh.invoke(obj, "Andy");
        }
        System.out.println("Method handle: " + (System.currentTimeMillis() - start) + " ms");


        start = System.currentTimeMillis();
        mh = MethodHandles.insertArguments(mh, 1, "Andy");
        for (int i = 0; i < 1000_000_00; ++i) {
            mh.invoke(obj);
        }
        System.out.println("Method handle with set arg: " + (System.currentTimeMillis() - start) + " ms");


        MethodHandle mh2 = MethodHandles.lookup().findStatic(MH1.class, "hello2", helloMtype);
        start = System.currentTimeMillis();
        for (int i = 0; i < 1000_000_00; ++i) {
            mh2.invoke("Andy");
        }
        System.out.println("Method handle static: " + (System.currentTimeMillis() - start) + " ms");
    }
}
