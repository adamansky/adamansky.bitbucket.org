package chereshnev;

import java.util.*;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

//this class use same algorithm with java.util.random for generation
//but it's not thread-safe and it doesn't use virtual methods
//so we can get same properties for generated numbers without overhead
class FastRandom
{
	private long seed = System.nanoTime();
    private static final Random rnd = new Random();
	public final int nextInt()
	{
//		seed = (seed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
//        return (int)(seed >>> 16);
        return rnd.nextInt(Integer.MAX_VALUE) - rnd.nextInt(Integer.MAX_VALUE);

	}
}

public class Main
{
	//if it's not ok to use custom random implementation uncomment this line
	//private final Random random = new Random();
	private final FastRandom random = new FastRandom();
	
	private final int N;
	private final int N_DEFAULT = 10000000;
	private final int KEY_COUNT;
	private final int KEY_COUNT_DEFAULT = 100000;
	private final String SORTED_FILE_NAME = "sorted";
	private final String NUMBER_OCCURENCES_FILE_NAME = "map.txt";
	private int[] array;
	
	public Main(String[] args)
	{
		int valueN = N_DEFAULT;
		int valueKeyCount = KEY_COUNT_DEFAULT;
		if (args.length > 0)
		{
			try
			{
				valueN = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException nfe)
			{
				valueN = N_DEFAULT;
			}
		}
		if (args.length > 1)
		{
			try
			{
				valueKeyCount = Integer.parseInt(args[1]);
			}
			catch (NumberFormatException nfe)
			{
				valueKeyCount = KEY_COUNT_DEFAULT;
			}

		}
		N = valueN <= 0 ? N_DEFAULT : valueN;
		KEY_COUNT = (valueKeyCount <= 0 || valueKeyCount > N) ? KEY_COUNT_DEFAULT : valueKeyCount;
		array = new int[N];
	}
	
	
	private int[] fillArrayAndGetKeys() throws InterruptedException
	{
		int[] keyArray = new int[KEY_COUNT];
		for (int i = 0; i < N; i++)
			array[i] = random.nextInt();
		
		for (int i = 0; i < KEY_COUNT; i++)
			 keyArray[i] = array[i];
		return keyArray;
	}
	
	private void readArray() throws IOException
	{
		FileInputStream f = new FileInputStream(SORTED_FILE_NAME);
		FileChannel ch = f.getChannel();
		MappedByteBuffer buffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, ch.size());
		final boolean isOptFormatFlag = buffer.get() == 1;
		if (isOptFormatFlag)
		{
            array[0] = Integer.MIN_VALUE + buffer.getShort();
			for (int i = 1; i < N; i++)
				array[i] = array[i - 1] + buffer.getShort();
			f.close();
		}
		else
		{
			for (int i = 0; i < N; i++)
				array[i] = buffer.getInt();
		}
		ArrayWriter.unmap(buffer);
		f.close();
	}
	
	private static void writeToFile(String fileName, byte[] byteArray, int length) throws IOException
	{
		RandomAccessFile file = new RandomAccessFile(fileName, "rw");
        file.setLength(0);
		file.getChannel().write(ByteBuffer.wrap(byteArray, 0, length));
		file.close();
	}
	
	private void countAndWriteOccurences(final int[] keyArray) throws IOException, InterruptedException
	{
		readArray();
		final int[] counts = new int[keyArray.length];
        ArrayHelper.introSort(keyArray);
		Thread[] threads = new Thread[2*Utils.getProcessors()];
		final int STEP = keyArray.length / threads.length;
		for (int i = 0; i < threads.length; i++)
		{
			final int index = i * STEP;
			threads[i] = new Thread(new Runnable()
			{
				public void run()
				{
					for (int i = index; i < index + STEP; i++)
						counts[i] = ArrayHelper.countOccurences(array, keyArray[i]);
				}
			});
			threads[i].start();
		}
		
		final int MAX_BYTES_PER_LINE = 20;
		byte[] byteArray = new byte[KEY_COUNT * MAX_BYTES_PER_LINE];
		int pos = 0;
		for (int i = 0; i < threads.length; i++)
		{
			final int index = i * STEP;
			threads[i].join();
			for (int j = index; j < index + STEP; j++)
			{
				byte[] bytes = (keyArray[j] + " : " + counts[j] + Utils.getNewLine()).getBytes();
				System.arraycopy(bytes, 0, byteArray, pos, bytes.length);
				pos += bytes.length;
			}
		}
		
		for (int i = threads.length * STEP; i < keyArray.length; i++)
		{
			counts[i] = ArrayHelper.countOccurences(array, keyArray[i]);
			byte[] bytes = (keyArray[i] + " : " + counts[i] + Utils.getNewLine()).getBytes();
			System.arraycopy(bytes, 0, byteArray, pos, bytes.length);
			pos += bytes.length;
		}
		writeToFile(NUMBER_OCCURENCES_FILE_NAME, byteArray, pos);
	}

	public int[] writeSortedArrayAndGetKeys() throws Exception
	{
		int[] keyArray = fillArrayAndGetKeys();
		ArrayWriter arrayWriter = new ArrayWriter(SORTED_FILE_NAME);
		ArrayHelper.introSort(array);
		arrayWriter.writeArrayCompact(array);
        if (arrayWriter.hasError()) {
            arrayWriter.writeArray(array);
        } else {
			arrayWriter.close();
        }
		
		return keyArray;
	}
	
	public static void benchmark(Main main) throws Exception
	{
		final int N_ITERATIONS = 5;
		double tMin = Double.MAX_VALUE;
		double tSum = 0;
		for (int i = 0; i < N_ITERATIONS; i++)
		{
			long start = System.currentTimeMillis();
			
			int[] keys = main.writeSortedArrayAndGetKeys();
			main.countAndWriteOccurences(keys);
			double t = (System.currentTimeMillis() - start);
			tMin = Math.min(tMin, t);
			tSum += t;
			System.out.println("Time: " + t);
		}
		System.out.println("Min: " + tMin);
		System.out.println("Average: " + tSum/N_ITERATIONS);
	}
	
	
	
	public static void main(String[] args) throws Exception
	{
        long start = System.currentTimeMillis();
		benchmark(new Main(args));
        System.out.println("T=" + (System.currentTimeMillis() - start));
    }
}

class ArrayWriter
{
	private int prevNumber = Integer.MIN_VALUE;
	private boolean error = false;
	private String fileName;
	RandomAccessFile file;
	
	public ArrayWriter(String fileName) throws IOException
	{
		this.fileName = fileName;
		file = new RandomAccessFile(fileName, "rw");
		file.setLength(0);
	}
	
	public boolean hasError()
	{
		return error;
	}
	
	public void writeArray(int[] array) throws IOException
	{
		final byte isOptFormatFlag = 0;
		file.setLength(0);
		MappedByteBuffer buffer = file.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 4*array.length + 1);
		buffer.put(isOptFormatFlag);
		for (int i = 0; i < array.length; i++)
			buffer.putInt(array[i]);
		unmap(buffer);
		file.close();
	}
	
	public void writeArrayCompact(int[] array) throws IOException
	{
		//write differences instead of elements of array
		//if array was generated using uniform random generator then
		//probability that differences could fit into short type is very high
		if (error)
			return;
		final byte isOptFormatFlag = 1;
		ByteBuffer buffer = ByteBuffer.allocate(2*array.length + 1);
		buffer.put(isOptFormatFlag);
		
		int dif = array[0] - Integer.MIN_VALUE;
		if (dif > Short.MAX_VALUE)
		{
			error = true;
			return;
		}
		buffer.putShort((short)dif);
		
		for (int i = 1; i < array.length; i++)
		{
			dif = array[i] - array[i - 1];
			if (dif > Short.MAX_VALUE)
			{
				error = true;
				return;
			}
			buffer.putShort((short)dif);
		}
		buffer.flip();
		file.getChannel().write(buffer);
		file.close();
	}
	
	public void close() throws IOException
	{
		file.close();
	}
	
	public static void unmap(MappedByteBuffer buffer)
	{
		sun.misc.Cleaner cleaner = ((sun.nio.ch.DirectBuffer)buffer).cleaner();
		cleaner.clean();
	}
}

class Utils
{
	private static final String NEWLINE = System.getProperty("line.separator");
	private static final int PROCESSORS = Runtime.getRuntime().availableProcessors();

	public static String getNewLine()
	{
		return NEWLINE;
	}
	public static int getProcessors()
	{
		return PROCESSORS;
	}
}