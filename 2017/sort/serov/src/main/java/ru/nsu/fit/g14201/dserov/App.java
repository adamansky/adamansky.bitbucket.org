package ru.nsu.fit.g14201.dserov;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.util.*;

public class App {

    private static String[] arr = new String[0];

    private static final int MAX_BUFSIZE = 1_073_741_824 / 8;

    public static void main( String[] args ) {

        partialSort();

        finalSort();


    }

    private static void finalSort() {
        Path outputFile = FileSystems.getDefault().getPath("output");
        try (SeekableByteChannel outputChannel = Files.newByteChannel(outputFile,
                StandardOpenOption.CREATE,
                StandardOpenOption.WRITE,
                StandardOpenOption.APPEND)) {
            Map<Integer, BufferedReader> readers = new HashMap<>(16);
            for (int i = 0; i < 16; i++) {
                readers.put(i, new BufferedReader(new FileReader("temp" + (i + 1))));
            }

            Map<Integer, String> strings = new HashMap<>(16);
            for (int i = 0; i < 16; i++) {
                String string = readers.get(i).readLine();
                strings.put(i, string);
            }


            while (!strings.isEmpty()) {
                boolean initial = true;

                String minString = null;
                int minIndex = 0;
                for (Map.Entry<Integer, String> entry : strings.entrySet()) {
                    if (initial) {
                        initial = false;
                        minString = entry.getValue();
                        minIndex = entry.getKey();
                    }
                    if (entry.getValue().compareTo(minString) < 0) {
                        minString = entry.getValue();
                        minIndex = entry.getKey();
                    }
                }

                ByteBuffer buffer = ByteBuffer.wrap((minString + "\n").getBytes());
                while (buffer.hasRemaining()) {
                    outputChannel.write(buffer);
                }

                strings.remove(minIndex);
                String nextString = readers.get(minIndex).readLine();
                if (nextString == null) {
                    readers.remove(minIndex);
                    Files.delete(Paths.get("temp" + (minIndex + 1)));
                } else {
                    strings.put(minIndex, nextString);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void partialSort() {
        Path inputFile = FileSystems.getDefault().getPath("input");

        long pos = 0L;
        try (SeekableByteChannel inputChannel = Files.newByteChannel(inputFile)) {
            inputChannel.position(pos);

            ByteBuffer buffer = ByteBuffer.allocate(MAX_BUFSIZE);
            String lastTail = "";

            for (int i = 0; i < 16; i++) {
                while (buffer.hasRemaining()) {
                    inputChannel.read(buffer);
                }

                int lastLineStart = buffer.position() - 1;
                while (lastLineStart >= 0 && buffer.get(lastLineStart) != '\n') {
                    lastLineStart--;
                }

                String newLastTail = "";
                if (lastLineStart >= 0) {
                    StringBuilder sb = new StringBuilder();
                    for (int j = lastLineStart + 1; j < buffer.position(); j++) {
                        sb.append((char)buffer.get(j));
                    }
                    newLastTail = sb.toString();
                }

                String[] strings = getStrings(lastTail, lastLineStart + 1, buffer, i == 15 ? newLastTail : null);
                lastTail = newLastTail;
                buffer.clear();
                pos += MAX_BUFSIZE;

                Arrays.parallelSort(strings);

                try (SeekableByteChannel tempChannel = Files.newByteChannel(Paths.get("temp" + (i + 1)),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.APPEND)) {

                    for (String string : strings) {
                        ByteBuffer stringBuffer = ByteBuffer.wrap(string.getBytes());
                        while (stringBuffer.hasRemaining()) {
                            tempChannel.write(stringBuffer);
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String[] getStrings(String lastTail, int end, ByteBuffer buffer, String last) {
        ArrayList<String> list = new ArrayList<>();
        StringBuilder sb = new StringBuilder(100);
        sb.append(lastTail);
        for (int i = 0; i < end; i++) {
            sb.append((char)buffer.get(i));
            if (buffer.get(i) == '\n') {
                list.add(sb.toString());
                sb.delete(0, sb.length());
            }
        }

        if (last != null) {
            list.add(last + "\n");
        }

        return list.toArray(arr);
    }
}
