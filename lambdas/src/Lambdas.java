import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.function.Supplier;

public class Lambdas {

    public static void greeting(Supplier smb) {
        System.out.println(smb.get());
    }

    public static void main(String[] args) throws Throwable {

        greeting(() -> {
            return "";
        });
    }

}

