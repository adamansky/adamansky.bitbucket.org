// ================ru/nsu/DefaultArgsKt.class =================
// class version 50.0 (50)
// access flags 0x31
public final class ru/nsu/DefaultArgsKt {


  // access flags 0x19
  public final static hello(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
  @Lkotlin/jvm/JvmOverloads;() // invisible
    @Lorg/jetbrains/annotations/NotNull;() // invisible, parameter 0
    @Lorg/jetbrains/annotations/NotNull;() // invisible, parameter 1
    @Lorg/jetbrains/annotations/Nullable;() // invisible, parameter 2
   L0
    ALOAD 0
    LDC "msg"
    INVOKESTATIC kotlin/jvm/internal/Intrinsics.checkParameterIsNotNull (Ljava/lang/Object;Ljava/lang/String;)V
    ALOAD 1
    LDC "friend1"
    INVOKESTATIC kotlin/jvm/internal/Intrinsics.checkParameterIsNotNull (Ljava/lang/Object;Ljava/lang/String;)V
   L1
    LINENUMBER 5 L1
    NEW java/lang/StringBuilder
    DUP
    INVOKESPECIAL java/lang/StringBuilder.<init> ()V
    LDC ""
    INVOKEVIRTUAL java/lang/StringBuilder.append (Ljava/lang/String;)Ljava/lang/StringBuilder;
    ALOAD 0
    INVOKEVIRTUAL java/lang/StringBuilder.append (Ljava/lang/String;)Ljava/lang/StringBuilder;
    BIPUSH 32
    INVOKEVIRTUAL java/lang/StringBuilder.append (C)Ljava/lang/StringBuilder;
    ALOAD 1
    INVOKEVIRTUAL java/lang/StringBuilder.append (Ljava/lang/String;)Ljava/lang/StringBuilder;
    INVOKEVIRTUAL java/lang/StringBuilder.toString ()Ljava/lang/String;
    ASTORE 3
   L2
    GETSTATIC java/lang/System.out : Ljava/io/PrintStream;
    ALOAD 3
    INVOKEVIRTUAL java/io/PrintStream.println (Ljava/lang/Object;)V
   L3
   L4
    LINENUMBER 6 L4
    ALOAD 2
    IFNULL L5
   L6
    LINENUMBER 7 L6
   L7
    GETSTATIC java/lang/System.out : Ljava/io/PrintStream;
    ALOAD 2
    INVOKEVIRTUAL java/io/PrintStream.println (Ljava/lang/Object;)V
   L8
   L5
    LINENUMBER 9 L5
    RETURN
   L9
    LOCALVARIABLE msg Ljava/lang/String; L0 L9 0
    LOCALVARIABLE friend1 Ljava/lang/String; L0 L9 1
    LOCALVARIABLE extra1 Ljava/lang/String; L0 L9 2
    MAXSTACK = 2
    MAXLOCALS = 4

  // access flags 0x1049
  public static synthetic bridge hello$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
  @Lkotlin/jvm/JvmOverloads;() // invisible
    ILOAD 3
    ICONST_2
    IAND
    IFEQ L0
   L1
    LINENUMBER 4 L1
    LDC "Andy"
    ASTORE 1
   L0
    ILOAD 3
    ICONST_4
    IAND
    IFEQ L2
    ACONST_NULL
    CHECKCAST java/lang/String
    ASTORE 2
   L2
    ALOAD 0
    ALOAD 1
    ALOAD 2
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    RETURN
    MAXSTACK = 3
    MAXLOCALS = 5

  // access flags 0x19
  public final static hello(Ljava/lang/String;Ljava/lang/String;)V
  @Lkotlin/jvm/JvmOverloads;() // invisible
    @Lorg/jetbrains/annotations/NotNull;() // invisible, parameter 0
    @Lorg/jetbrains/annotations/NotNull;() // invisible, parameter 1
   L0
    ALOAD 0
    ALOAD 1
    ACONST_NULL
    ICONST_4
    ACONST_NULL
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello$default (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    RETURN
   L1
    LOCALVARIABLE msg Ljava/lang/String; L0 L1 0
    LOCALVARIABLE friend1 Ljava/lang/String; L0 L1 1
    MAXSTACK = 5
    MAXLOCALS = 2

  // access flags 0x19
  public final static hello(Ljava/lang/String;)V
  @Lkotlin/jvm/JvmOverloads;() // invisible
    @Lorg/jetbrains/annotations/NotNull;() // invisible, parameter 0
   L0
    ALOAD 0
    ACONST_NULL
    ACONST_NULL
    BIPUSH 6
    ACONST_NULL
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello$default (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    RETURN
   L1
    LOCALVARIABLE msg Ljava/lang/String; L0 L1 0
    MAXSTACK = 5
    MAXLOCALS = 1

  // access flags 0x19
  public final static main([Ljava/lang/String;)V
    @Lorg/jetbrains/annotations/NotNull;() // invisible, parameter 0
   L0
    ALOAD 0
    LDC "args"
    INVOKESTATIC kotlin/jvm/internal/Intrinsics.checkParameterIsNotNull (Ljava/lang/Object;Ljava/lang/String;)V
   L1
    LINENUMBER 12 L1
    LDC "Hello"
    ACONST_NULL
    ACONST_NULL
    BIPUSH 6
    ACONST_NULL
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello$default (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
   L2
    LINENUMBER 13 L2
    LDC "Hello"
    LDC "Pit"
    ACONST_NULL
    ICONST_4
    ACONST_NULL
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello$default (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
   L3
    LINENUMBER 14 L3
    LDC "Hello"
    ACONST_NULL
    LDC "Extra"
    ICONST_2
    ACONST_NULL
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello$default (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
   L4
    LINENUMBER 15 L4
    LDC "Hello"
    LDC "Extra"
    LDC "John"
    ASTORE 1
    ASTORE 2
    ALOAD 1
    ALOAD 2
    INVOKESTATIC ru/nsu/DefaultArgsKt.hello (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
   L5
    LINENUMBER 16 L5
    RETURN
   L6
    LOCALVARIABLE args [Ljava/lang/String; L0 L6 0
    MAXSTACK = 5
    MAXLOCALS = 3

  @Lkotlin/Metadata;(mv={1, 1, 7}, bv={1, 0, 2}, k=2, d1={"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u001a&\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003H\u0007\u001a\u0019\u0010\u0006\u001a\u00020\u00012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0008\u00a2\u0006\u0002\u0010\u0009\u00a8\u0006\n"}, d2={"hello", "", "msg", "", "friend1", "extra1", "main", "args", "", "([Ljava/lang/String;)V", "production sources for module kotlin"})
  // compiled from: DefaultArgs.kt
  // debug info: SMAP
DefaultArgs.kt
Kotlin
*S Kotlin
*F
+ 1 DefaultArgs.kt
ru/nsu/DefaultArgsKt
*L
1#1,16:1
*E

}


// ================META-INF/production sources for module kotlin.kotlin_module =================
            

ru.nsu
DefaultArgsKt

