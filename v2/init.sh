#!/bin/bash
set -e

bundle config --local github.https true
bundle --path=.bundle/gems --binstubs=.bundle/.bin

if [[ ! -d reveal.js ]]; then
  git clone -b 3.7.0 --depth 1 https://github.com/hakimel/reveal.js.git
fi

# bundle exec asciidoctor-revealjs ./KotlinPrimer.adoc
# bundle exec asciidoctor-revealjs ./GCE.adoc
# bundle exec asciidoctor-revealjs ./GRAALVM.adoc
bundle exec asciidoctor-revealjs ./MODULES.adoc


