@file:JvmName("Ranges")
package ru.nsu

fun loopWithRange() {
    for (i in 5 downTo 1 step 2) {
        print(i)
    }
}

