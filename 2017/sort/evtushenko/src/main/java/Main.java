import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public final class Main {
    private static final int size1 = 2147483646;
    private static final int items = (int) (size1 / 77L) + 1;

    public static void main(String[] args) throws IOException, InterruptedException {
        checkArgs(args);
        long start = System.currentTimeMillis();
        File inputFile = new File(args[0]);
        MappedByteBuffer data = new RandomAccessFile(inputFile, "r")
                .getChannel()
                .map(FileChannel.MapMode.READ_ONLY, 0, size1);

        byte[] lastWord = readLastLine(inputFile);
        Integer[] indexes = new Integer[items];
        for (int i = 0; i < items; i++) {
            indexes[i] = i;
        }

        Comparator<Integer> integerComparator = (o1, o2) -> {
            if (o1 == items - 1) {
                for (int i = 0; i < 2; i++) {
                    if (lastWord[i] < data.get(77 * o2 + i)) {
                        return -1;
                    } else if (lastWord[i] > data.get(77 * o2 + i)) {
                        return 1;
                    }
                }
                return -1;
            } else if (o2 == items - 1) {
                for (int i = 0; i < 2; i++) {
                    if (data.get(77 * o1 + i) < lastWord[i]) {
                        return -1;
                    } else if (data.get(77 * o1 + i) > lastWord[i]) {
                        return 1;
                    }
                }
                return 1;
            } else {
                for (int i = 0; i < 76; i++) {
                    if (data.get(77 * o1 + i) < data.get(77 * o2 + i)) {
                        return -1;
                    } else if (data.get(77 * o1 + i) > data.get(77 * o2 + i)) {
                        return 1;
                    }
                }
                return 0;
            }
        };

        Arrays.parallelSort(indexes, integerComparator);

        File outputFile = new File(args[1]);
        MappedByteBuffer out = new RandomAccessFile(outputFile, "rw")
                .getChannel()
                .map(FileChannel.MapMode.READ_WRITE, 0, size1);
        MappedByteBuffer out2 = new RandomAccessFile(outputFile, "rw")
                .getChannel()
                .map(FileChannel.MapMode.READ_WRITE, size1, 2);
        int numOfThreads = Runtime.getRuntime().availableProcessors();
        int wordsPerThread = items / numOfThreads;
        int shortWordPosition = Arrays.asList(indexes).indexOf(items - 1);
        ArrayList<Thread> threads = new ArrayList<>(numOfThreads);
        Thread a;
        for (int threadNum = 0; threadNum < numOfThreads - 1; threadNum++) {
            int curThreadNum = threadNum;
            a = new Thread(() -> {
                for (int p = wordsPerThread * curThreadNum; p < wordsPerThread * (curThreadNum + 1); p++) {
                    if (indexes[p] == items - 1) {
                        for (int byteNum = 0; byteNum < 3; byteNum++) {
                            out.put(p * 77 + byteNum, lastWord[byteNum]);
                        }
                    } else {
                        move(data, indexes[p], out, shortWordPosition, p);
                    }
                }
            });
            threads.add(a);
            a.start();
        }
        a = new Thread(() -> {
            int p;
            for (p = wordsPerThread * (numOfThreads - 1); p < items - 1; p++) {
                move(data, indexes[p], out, shortWordPosition, p);
            }
            for (int byteNum = 0; byteNum < 74; byteNum++) {
                out.put(p * 77 + byteNum - 74, data.get(indexes[p] * 77 + byteNum));
            }
            for (int byteNum = 74; byteNum < 76; byteNum++) {
                out2.put(byteNum - 74, data.get(indexes[p] * 77 + byteNum));
            }
        });
        threads.add(a);
        a.start();
        for (Thread t : threads) {
            t.join();
        }
        System.out.println(System.currentTimeMillis()-start);
    }

    private static void move(MappedByteBuffer data, Integer index1, MappedByteBuffer out, int shortWordPosition, int p) {
        for (int byteNum = 0; byteNum < 77; byteNum++) {
            int index = p * 77 + byteNum;
            if (p > shortWordPosition) {
                index -= 74;
            }
            out.put(index, data.get(index1 * 77 + byteNum));
        }
    }

    private static byte[] readLastLine(File inputFile) throws IOException {
        MappedByteBuffer data2 = new RandomAccessFile(inputFile, "r")
                .getChannel()
                .map(FileChannel.MapMode.READ_ONLY, size1, 2);
        byte[] additional = new byte[3];
        data2.get(additional, 0, 2);
        additional[2] = '\n';
        return additional;
    }

    private static void checkArgs(String[] args){
        if (args == null || args.length != 2){
            System.out.println("Usage:java Sort input output");
            System.exit(1);
        }
        File inputFile = new File(args[0]);
        if (!inputFile.exists() || inputFile.isDirectory()){
            System.out.println("Input file does not exist or it is a directory");
        }
        File outputFile = new File(args[1]);
        try {
            outputFile.createNewFile();
        } catch (IOException e) {
            System.out.println("Can not create output file");
        }

    }

}
