package nsu.fit.javaperf;

import java.io.Console;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

/**
 * JVM load-set tests
 */
public class LoadSet {

    static final Object rlock = new Object();

    static List<Task> tasks = new ArrayList(); //Tasks for runner

    static GarbageProducer garbageProducer;

    static CPULoader cpuLoader;


    static class CPULoader extends Thread {

        static final int DATA_COUNT = 10000;

        static final int DATA_STR_MLEN = 128;

        volatile boolean stopped;

        final String[] data;

        final Random rnd;

        CPULoader() {
            super("CPULoader");
            rnd = new Random(System.currentTimeMillis());
            data = new String[DATA_COUNT];
            for (int i = 0; i < DATA_COUNT; ++i) {
                int len = rnd.nextInt(DATA_STR_MLEN - 1) + 1;
                StringBuilder sb = new StringBuilder(len);
                for (int j = 0; j < len; ++j) {
                    sb.append(Character.valueOf((char) rnd.nextInt(Character.MAX_VALUE)));
                }
                data[i] = sb.toString();
            }
        }

        int action(int l) {
            int k = 0;
            for (int j = 0; j < data[l].length(); ++j) {
                k += (int) rnd.nextInt(data[l].charAt(j) + 1);
            }
            return k;
        }

        public void run() {
            while (!stopped) {
                int k = 0;
                for (int j = 0; j < 10; ++j) {
                    for (int i = 0; i < DATA_COUNT; ++i) {
                        k += action(i);
                    }
                    if (stopped) break;
                }
            }
        }

        public void scheduleStop() {
            stopped = true;
        }
    }


    static class Garbage {
        private List list = new LinkedList();

        Garbage() {
            for (int i = 0; i < 5; i++) {
                list.add(new byte[1]);
            }
        }
    }

    static class GarbageProducer extends Thread {

        private List<Garbage> garbageStorage;

        AtomicLong garbageRemovePeriod = new AtomicLong(150);

        AtomicLong garbageAmount = new AtomicLong(50000);

        private volatile boolean stopped;

        GarbageProducer() {
            super("GarbageProducer");
        }

        public void run() {
            garbageStorage = new ArrayList<>();
            try {
                // период удаления созданного мусора
                long lastGarbageRemoveTime = System.currentTimeMillis();

                // количество мусора, создаваемого за одну итерацию
                while (!stopped) {
                    for (int i = 0; i < garbageAmount.longValue(); i++) {
                        garbageStorage.add(new Garbage());
                    }
                    if (System.currentTimeMillis() > (lastGarbageRemoveTime + garbageRemovePeriod.longValue())) {
                        garbageStorage.clear();
                        Thread.sleep(100);
                        lastGarbageRemoveTime = System.currentTimeMillis();
                    }
                }
                garbageStorage.clear();
                garbageStorage = null;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void scheduleStop() {
            stopped = true;
        }
    }


    static class Task {
        final String name;
        final String cmdline;

        Task(String name) {
            this.name = name;
            this.cmdline = name;
        }

        Task(String name, String cmdline) {
            this.name = name;
            this.cmdline = cmdline;
        }

        public String toString() {
            final StringBuilder sb = new StringBuilder("Task{");
            sb.append("name='").append(name).append('\'');
            sb.append(", cmdline='").append(cmdline).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    static class TaskRunner implements Runnable {

        TaskRunner() {
        }

        void stop() {
            System.err.printf("[%s]: Clearing all allocated memory\n", Thread.currentThread().getName());
            if (garbageProducer != null) {
                garbageProducer.scheduleStop();
                garbageProducer = null;
            }
            if (cpuLoader != null) {
                cpuLoader.scheduleStop();
                cpuLoader = null;
            }
        }

        static void yg(String cmd) {
            Scanner s = new Scanner(new StringReader(cmd));
            String t = s.next();
            long amount = 0;
            if (s.hasNextLong()) {
                amount = s.nextLong();
            }
            long rperiod = 0;
            if (s.hasNextLong()) {
                rperiod = s.nextLong();
            }
            if (rperiod > 0) {
                garbageProducer.garbageRemovePeriod.set(rperiod);
            }
            if (amount > 0) {
                garbageProducer.garbageAmount.set(amount);
            }
            if (garbageProducer == null || !garbageProducer.isAlive()) {
                garbageProducer = new GarbageProducer();
                garbageProducer.start();
            }
            System.err.printf("Young garbage generator: amount: %d, rm period: %d\n",
                              garbageProducer.garbageAmount.longValue(),
                              garbageProducer.garbageRemovePeriod.longValue());
        }


        static void cpu(String cmd) {
            Scanner s = new Scanner(new StringReader(cmd));
            String t = s.next();
            String subcmd = s.hasNext() ? s.next() : null;
            if ("stop".equals(subcmd)) {
                if (cpuLoader != null) {
                    System.err.println("Stopping cpuload generator");
                    cpuLoader.scheduleStop();
                }
            } else {
                if (cpuLoader == null || !cpuLoader.isAlive()) {
                    cpuLoader = new CPULoader();
                    cpuLoader.start();
                }
            }
        }

        public void run() {
            boolean active = true;
            while (active) {
                Task task;
                synchronized (rlock) {
                    if (tasks.isEmpty()) {
                        try {
                            rlock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            continue;
                        }
                    }
                    if (!tasks.isEmpty()) {
                        task = tasks.remove(0);
                    } else {
                        continue;
                    }
                }
                String tn = task.name;
                switch (tn) {
                    case "exit":
                        active = false;
                        break;
                    case "stop":
                        stop();
                        break;
                    case "yg":
                        yg(task.cmdline);
                        break;
                    case "ygs":
                        if (garbageProducer != null) {
                            System.err.println("Stopping young garbage generator");
                            garbageProducer.scheduleStop();
                        }
                        break;
                    case "cpu":
                        cpu(task.cmdline);
                        break;
                }
            }
        }
    }

    static void sendTask(Task t) {
        synchronized (rlock) {
            tasks.add(t);
            rlock.notify();
        }
    }

    static void info(Console c, boolean memonly) {
        PrintWriter w = c.writer();
        Runtime rt = Runtime.getRuntime();
        w.printf("Max memory: %d (%dMb)\n", rt.maxMemory(), (int) (rt.maxMemory() / (1024 * 1024)));
        w.printf("Total memory: %d (%dMb)\n", rt.totalMemory(), (int) (rt.totalMemory() / (1024 * 1024)));
        w.printf("Free memory: %d (%dMb)\n", rt.freeMemory(), (int) (rt.freeMemory() / (1024 * 1024)));


        if (!memonly) {
            RuntimeMXBean rtMx = ManagementFactory.getRuntimeMXBean();
            List<String> jargs = rtMx.getInputArguments();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < jargs.size(); ++i) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(jargs.get(i));
            }
            w.printf("[%s] %s %s %s (%s)\n", rtMx.getName(), rtMx.getVmName(), rtMx.getVmVendor(), rtMx.getSpecVersion(), rtMx.getVmVersion());
            w.println("JVM args: " + sb);
        }
        if (garbageProducer != null) {
            w.printf("Young garbage generator: %s, amount: %d, rm period: %d\n",
                     garbageProducer.isAlive() ? "ACTIVE" : "NOT ACTIVE",
                     garbageProducer.garbageAmount.longValue(),
                     garbageProducer.garbageRemovePeriod.longValue());
        } else {
            w.printf("Young garbage generator: NOT ACTIVE\n");
        }
        w.printf("CPU loader: %s\n", (cpuLoader != null && cpuLoader.isAlive()) ? "ACTIVE" : "NOT ACTIVE");
        w.flush();
    }

    static void help(Console c) {
        PrintWriter w = c.writer();
        w.println("\t Available commmands:\n");
        w.println("\t?|h|help: Print this help.");
        w.println("\tq|quit|exit: Exit peogram.");
        w.println("\ti|info: Show brief system info.");
        w.println("\tgc: Run GC.");
        w.println("\tstop|stopall: Stop all generators.");
        w.println("\tyg [amount] [removeperiod]: Start generate young garbage. By default amount: 50000, removeperiod: 150ms");
        w.println("\tyg stop: Stop young garbage generator");
        w.println("\tcpu start|stop: Start/Stop CPU load generator.");
        w.flush();
    }


    static void cpurun(int sec) throws Exception {
        //System.err.println("Run CPU test " + sec);
        if (cpuLoader != null) {
            cpuLoader.scheduleStop();
        }
        cpuLoader = new CPULoader();
        cpuLoader.start();
        Thread.sleep(100);
        cpuLoader.join(sec * 1000 - 100);
        cpuLoader.scheduleStop();
        Thread.yield();
        cpuLoader.join();
    }

    public static void main(String[] args) throws Exception {
        List<String> alist = Arrays.asList(args);
        int ind = alist.indexOf("-cpu");
        if (ind != -1) {
            int sec = 60;
            if (alist.size() > ++ind) {
                sec = Integer.parseInt(alist.get(ind));
            }
            cpurun(sec > 0 ? sec : 60);
            return;
        }


        Console c = System.console();
        if (c == null) {
            System.err.println("Java program cannot open interactive console");
            System.exit(1);
        }

        Thread tf = new Thread(new TaskRunner());
        tf.setName("TaskThread");
        tf.start();

        PrintWriter w = c.writer();
        w.println("Welcome to JVM load set.");
        String cmd;
        while ((cmd = c.readLine("> ")) != null) {
            cmd = cmd.trim();
            switch (cmd) {
                case "?":
                case "h":
                case "help":
                    help(c);
                    break;
                case "q":
                case "quit":
                case "exit":
                    if (garbageProducer != null) {
                        garbageProducer.scheduleStop();
                        Thread.yield();
                    }
                    if (cpuLoader != null) {
                        cpuLoader.scheduleStop();
                        Thread.yield();
                    }
                    if (cpuLoader != null && cpuLoader.isAlive()) {
                        cpuLoader.join(1000);
                    }
                    if (garbageProducer != null && garbageProducer.isAlive()) {
                        garbageProducer.join(1000);
                    }
                    sendTask(new Task("exit"));
                    tf.join(1000);
                    System.exit(0);
                    break;
                case "i":
                case "info":
                    info(c, false);
                    break;
                case "gc":
                    w.println("Before GC:");
                    info(c, true);
                    for (int i = 0; i < 2; ++i) {
                        Runtime.getRuntime().gc();
                        Thread.sleep(100);
                    }
                    w.println("After GC:");
                    info(c, true);
                    break;
                case "stopall":
                    sendTask(new Task("stop"));
                    break;
                case "stop":
                    sendTask(new Task(cmd));
                    break;
                default:
                    if (cmd.startsWith("yg")) {
                        if (cmd.matches("yg\\s+stop")) {
                            sendTask(new Task("ygs", cmd));
                        } else {
                            sendTask(new Task("yg", cmd));
                        }
                    } else if (cmd.startsWith("cpu ")) {
                        sendTask(new Task("cpu", cmd));
                    }
                    break;

            }
        }
    }
}
