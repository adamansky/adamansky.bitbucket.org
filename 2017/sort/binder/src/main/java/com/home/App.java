package com.home;

import java.io.*;
import java.util.*;

public class App
{
    public static void main( String[] args )
    {
        try
        {
            long time = System.nanoTime();
            File tmpFile = new File("tmp.data");
            if (tmpFile.exists())
            {
                tmpFile.delete();
                tmpFile.createNewFile();
            }

            final int lineSize = 77;
//            int numOfStrings = 512 * 1024 / lineSize;
            final int numOfStrings = 256 * 1024 * 1024 / lineSize;
            int parts = 0;

            try (FileWriter fileWriter = new FileWriter(tmpFile, false))
            {
                try (RandomAccessFile raf = new RandomAccessFile("test.data", "r"))
                {
                    String[] strings = new String[numOfStrings];
                    int n = numOfStrings;
                    boolean finished = false;

                    while (!finished)
                    {
                        for(int i = 0; i < numOfStrings; i++)
                        {
                            String tmp = raf.readLine();

                            if (tmp == null)
                            {
                                finished = true;
                                n = i;
                                break;
                            }

                            strings[i] = tmp + System.lineSeparator();
                        }

                        if (n != 0)
                        {
                            parts++;
//                            System.out.println("Parts: " + parts);
                        }

                        Arrays.parallelSort(strings);

                        for (int i = 0; i < n; i++)
                        {
                            fileWriter.write(strings[i]);
                        }
                        fileWriter.flush();
                    }
                }
            }

            File resFile = new File("res.data");
            if (resFile.exists())
            {
                resFile.delete();
                resFile.createNewFile();
            }

            try (RandomAccessFile raf = new RandomAccessFile(tmpFile, "r"))
            {
                try (FileWriter fw = new FileWriter(resFile, false))
                {
                    boolean finished = false;
                    String[] strings = new String[parts];
                    Integer[] offsets = new Integer[parts];
                    for (int i = 0; i < parts; i++)
                    {
                        offsets[i] = 0;
                        long pos = (long)i * (long)numOfStrings * (long)(lineSize + 1);
//                        System.out.println("#" + i + " seek pos: " + pos);
                        raf.seek(pos);
                        strings[i] = raf.readLine() + System.lineSeparator();
                    }

                    while (!finished)
                    {
                        List<String> stringList = new ArrayList<>(Arrays.asList(strings));
                        while (stringList.contains(null))
                        {
                            stringList.remove(null);
                        }
                        String min = Collections.min(stringList);
                        fw.write(min);

                        for (int i = 0; i < parts; i++)
                        {
                            if ((strings[i] != null) && strings[i].equals(min))
                            {
                                offsets[i]++;
                                if (offsets[i] == numOfStrings)
                                {
                                    offsets[i] = -1;
                                    strings[i] = null;
                                }
                                else
                                {
                                    long pos = ((long)i * (long)numOfStrings + (long)offsets[i]) * (long)(lineSize + 1);
                                    raf.seek(pos);
                                    String tmp = raf.readLine();
                                    if ((tmp == null) && (i == (parts - 1)))
                                    {
                                        offsets[i] = -1;
                                        strings[i] = null;
                                    }
                                    else
                                    {
                                        strings[i] = tmp + System.lineSeparator();
                                    }
                                }

                                break;
                            }
                        }

                        finished = true;
                        for (int i = 0; i < parts; i++)
                        {
                            if (offsets[i] != -1)
                            {
                                finished = false;

                                break;
                            }
                        }
                    }

                    fw.flush();
                }
            }

            System.out.println("Sorted! It took " + ((System.nanoTime() - time) / 1000000000) + " seconds!");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
