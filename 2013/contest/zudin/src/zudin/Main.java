package zudin;

import java.io.*;
import java.util.*;

public class Main {
    private static int numberOfIntegersToWrite = 10000000;
    private static int numberOfIntegersToGet = 100000;
    private static int IterationStep = 1;
    static int INT_MAX = Integer.MAX_VALUE;
    static int INT_MIN = Integer.MIN_VALUE;
    static ArrayList<Integer> list = new ArrayList<Integer>();
    static ArrayList<Integer> getlist = new ArrayList<Integer>();
    static Map<Integer, Integer> Table = new HashMap<Integer, Integer>();
    public static void main(String[] args) throws IOException {
        long timeout= System.currentTimeMillis();
        //write random sorted integers to file "output.txt"
        Random random = new Random();
        for (int i = 0; i < numberOfIntegersToWrite; i += IterationStep) {
            list.add(RandomInteger(INT_MIN, INT_MAX, random));
        }
        Collections.sort(list);
        PrintWriter writer = new PrintWriter("output.txt");
        PrintWriter answerWriter = new PrintWriter("answer.txt");

        for (Integer i: list){
            writer.println(i);
        }
        list.clear();
        writer.close();
        //get 100000 random integers from file "output.txt"
        Scanner file = new Scanner(new File("output.txt"));

        for (int i = 0; i < numberOfIntegersToGet; i += IterationStep) {
            list.add(RandomInteger(0, numberOfIntegersToWrite, random));
        }

        Collections.sort(list);
        int j = 0;
        for (Integer i: list){
            for( ; j < i - 1; j++){
                int a = file.nextInt();
                if(Table.containsKey(a))
                {
                    Table.put(a,Table.get(a) + 1);
                }
                else
                {
                    Table.put(a, 1);
                }
            }
            int a = file.nextInt();
            if(Table.containsKey(a))
            {
                Table.put(a,Table.get(a) + 1);
            }
            else
            {
                Table.put(a, 1);
            }
            getlist.add(a);
            j++;
        }
        for (Integer i: getlist){
            int value = Table.get(i);
            answerWriter.println(i + " - " + value);
        }
        answerWriter.close();
        timeout = System.currentTimeMillis() - timeout;
        System.out.println("T=" + timeout);
    }

    private static int RandomInteger(int aStart, int aEnd, Random aRandom) throws IOException {
        long range = (long)aEnd - (long)aStart + 1;
        long fraction = (long)(range * aRandom.nextDouble());
        int randomNumber =  (int)(fraction + aStart);
        return randomNumber;
    }
}