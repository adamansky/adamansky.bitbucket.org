GENERATE INPUT:
base64 /dev/urand | head -c 2147483648 > input

COMPILE:
mvn install

RUN:
java -jar -Xmx1G target/FileSorter-1.0-SNAPSHOT.jar input output

CHECK IF SORTED:
cat output | LC_ALL=C sort -c