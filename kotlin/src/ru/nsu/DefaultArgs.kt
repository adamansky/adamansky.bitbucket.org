package ru.nsu

@JvmOverloads
fun hello(msg: String, friend1: String = "Andy", extra1: String? = null) {
    println("$msg $friend1")
    if (extra1 != null) {
        println(extra1)
    }
}

fun main(args: Array<String>) {
    hello("Hello")
    hello("Hello", "Pit")
    hello("Hello", extra1 = "Extra")
    hello("Hello", extra1 = "Extra", friend1 = "John")
}