package egorova;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: vvego_000
 * Date: 25.11.13
 * Time: 0:07
 * To change this template use File | Settings | File Templates.
 */
public class FrequencyTable {
    /*Table with the number of each integer*/
    private TreeMap<Integer, Integer> freqMap = new TreeMap<Integer, Integer>();
    private int keyNumber = 0;     //number of integers for keys
    private int integerNumber = 0; //number of all integer in the source file

    public FrequencyTable(int integerNum, int keyNum) {
        keyNumber = keyNum;
        integerNumber = integerNum;
    }

    int binarySearch(IntBuffer buffer, int value) {
        int first = 0;
        int last = integerNumber - 1;
        while (first <= last) {
            int avrg = (first + last) / 2;
            int avrgVal = buffer.get(avrg);
            if (value < avrgVal) {
                last = avrg;
                continue;
            }
            if (value > avrgVal) {
                first = avrg;
                continue;
            }
            return avrg;
        }
        return -1;
    }

    void generateFrequencyTable(String integersFileName, String tableFileName, int[] keysArray) throws Exception {
        FileChannel readFileChannel = new RandomAccessFile(new File(integersFileName), "rw").getChannel();
        FileChannel writeFileChannel = new RandomAccessFile(new File(tableFileName), "rw").getChannel();
        ByteBuffer buffer = readFileChannel.map(FileChannel.MapMode.READ_ONLY, 0, readFileChannel.size());
        ByteBuffer outBuffer = writeFileChannel.map(FileChannel.MapMode.READ_WRITE, 0, keyNumber * 12);
        IntBuffer buf = buffer.asIntBuffer();
        /*Uncomment to see content of the file with sorted integers and
        to see the table (integer - number of integers)*/
        /*BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tabl.txt")));
        BufferedWriter writer1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("inp1.txt")));
        for (int i = 0; i < integerNumber; i++) {
            writer1.write(String.valueOf(buf.get(i)) + "\n");
        } */
        for (int key : keysArray) {
            int count = 0;
            int index = binarySearch(buf, key);
            int index1 = index;
            while (index1 < integerNumber && buf.get(index1) == key) {
                count++;
                index1++;
            }
            index1 = index - 1;
            while (index1 >= 0 && buf.get(index1) == key) {
                count++;
                index1--;
            }
            //writer.write(key + " " + count + "\n");
            outBuffer.putInt(key);
            outBuffer.putChar(' ');
            outBuffer.putInt(count);
        }
        /*writer1.close();
        writer.close();*/
    }
}