package ru.nsu.sample

import java.io.FileWriter

fun main() {
    FileWriter("myfile.txt").use { file ->
        file.write("Hello!!!")
    }
}