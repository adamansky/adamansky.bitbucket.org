package ru.nsu;

import kotlin.Metadata;
import kotlin.jvm.JvmName;
import kotlin.ranges.IntProgression;
import kotlin.ranges.RangesKt;

@Metadata(
   mv = {1, 1, 7},
   bv = {1, 0, 2},
   k = 2,
   d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u001a\u0006\u0010\u0000\u001a\u00020\u0001¨\u0006\u0002"},
   d2 = {"loopWithRange", "", "production sources for module kotlin"}
)
@JvmName(
   name = "Ranges"
)
public final class Ranges {
   public static final void loopWithRange() {
      IntProgression var10000 = RangesKt.step(RangesKt.downTo(5, 1), 2);
      int i = var10000.getFirst();
      int var1 = var10000.getLast();
      int var2 = var10000.getStep();
      if (var2 > 0) {
         if (i > var1) {
            return;
         }
      } else if (i < var1) {
         return;
      }

      while(true) {
         System.out.print(i);
         if (i == var1) {
            return;
         }

         i += var2;
      }
   }
}
