package nsu.fit.javaperf;

/**
 * Deadlock showcase.
 *
 * Usage: java -cp out/testgcpause.jar nsu.fit.javaperf.TestDeadLock
 */
public class TestDeadLock {

    static final Object lock1 = new Object();

    static final Object lock2 = new Object();

    public static void main(String[] args) throws Exception {

        new Thread(new Runnable() {
            public void run() {
                synchronized (lock2) {  //Lock2
                    System.err.println("t1: lock2");
                    try {
                        System.err.println("t1: lock2.wait()");
                        lock2.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    synchronized (lock1) { //Lock1
                        System.err.println("t1: lock1");
                    }
                }
            }
        }).start();

        Thread.sleep(100);

        new Thread(new Runnable() {
            public void run() {
                synchronized (lock1) {
                    System.err.println("t2: lock1");
                    synchronized (lock2) {
                        System.err.println("t2: lock2.notify");
                        lock2.notify();
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    synchronized (lock2) {
                        System.err.println("t2: lock2");
                    }
                }
            }
        }).start();
    }
}
