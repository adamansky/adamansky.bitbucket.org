package nsu.fit.javaperf.lab5;

import java.util.ArrayList;
import java.util.List;

/**
 * Use {@link java.lang.reflect.Proxy}
 */
public class Lab5 {

    static <T> T setLogging(final T obj) {
        //todo use java.lang.reflect.Proxy(InvocationHandler)
        //todo (http://docs.oracle.com/javase/7/docs/api/java/lang/reflect/Proxy.html)
        //
        //todo get all interfaces: obj.getClass().getInterfaces()
        //todo Intercept interface method with java.lang.reflect.InvocationHandler
        //todo Log each interface call into System.out
        //
        //todo OUTPUT EXAMPLE:
        //todo      CALLING:  java.lang.Runnable#run()
        //todo      RETURN VAL: null

        return (T) obj;
    }

    public static void main(String[] args) {

        Runnable obj = new Runnable() {
            public void run() {
                System.out.println("Hello I'm running...");
            }
        };
        System.out.println("============= First example ===============");
        obj = setLogging(obj);
        obj.run();


        System.out.println("============ Second example ==============");
        List<String> list = setLogging(new ArrayList<String>());
        list.add("One");
        list.add("Two");
        list.add("Three");
        for (String v : list) {
            System.out.println("V=" + v);
        }
        list.contains("One");
        list.contains("Five");
        list.clear();
        list.hashCode();
    }
}
