package ru.nsu

interface A {

    fun msg(): String

    fun show() {
        println(this.msg())
    }
}

class B : A {
    override fun msg(): String {
        return "B Class"
    }
}
