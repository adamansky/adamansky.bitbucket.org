import java.util.function.Supplier;

public class Lambdas {
   int state = 1;

   public static void greeting(Supplier var0) {
      System.out.println(var0.get());
   }

   public static void main(String[] var0) throws Throwable {
      greeting("Andy"::toString);
      greeting("AKK"::intern);
   }
}
