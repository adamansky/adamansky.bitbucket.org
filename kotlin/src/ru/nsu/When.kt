package ru.nsu

fun main() {
    val key = "foo"
    val ret = when (key) {
        "foo" -> "It is a foo"
        "bar" -> "It is a bar"
        else -> "I don't know it"
    }
    print("ret=${ret}")
}