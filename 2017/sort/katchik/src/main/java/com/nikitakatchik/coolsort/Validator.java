package com.nikitakatchik.coolsort;

import java.io.BufferedReader;
import java.io.FileReader;

public class Validator {
    public static void main(final String[] args) throws Exception {
        int error = 0;
        try (final BufferedReader reader = new BufferedReader(new FileReader("out.txt"))) {
            String prev = null;
            String current;
            while ((current = reader.readLine()) != null) {
                if (prev != null && current.compareTo(prev) < 0) {
                    System.out.println("Error at \n" + prev + "\nvs\n" + current);
                    error++;
                }
                prev = current;
            }
        }
        System.out.println("Total problems: " + error);
    }
}
