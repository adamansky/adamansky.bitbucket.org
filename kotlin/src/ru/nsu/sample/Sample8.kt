package ru.nsu.sample

class Person2(val name: String) {
    lateinit var email: String
}

fun main() {
    Person2(name = "Bob").apply {
        email = "bob@bobby.me"
    }
}

fun Person2.asJson(): String = """
        {
          "name": ${name},
          "email": ${email}
        }
        """

