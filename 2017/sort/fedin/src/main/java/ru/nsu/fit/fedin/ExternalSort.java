package ru.nsu.fit.fedin;

import java.io.*;
import java.util.*;

public class ExternalSort {

    private int numberOfBLocks(long size, long sizeOfFile){
        System.out.println(size);
        long first = sizeOfFile;
        return (int) Math.ceil((float)size / first );
    }
    public void sort(String mainFile) throws IOException {
        File main = new File(mainFile);
        BufferedReader rd = new BufferedReader(new InputStreamReader(new FileInputStream(mainFile)));
        List<String> box  = new ArrayList<>();
        Comparator<String> comparator = (o1, o2) -> o1.compareTo(o2);

        String line;
//        long sizeOfFile = Runtime.getRuntime().freeMemory()/2;
        long sizeOfFile = main.length()/14;
        int numberOfBlocks = numberOfBLocks(main.length(),sizeOfFile);
        PriorityQueue<BinaryFileBuffer> pq = new PriorityQueue<>(3,
                (i, j) -> comparator.compare(i.peek(), j.peek())
        );
        for(int i = 0; i < numberOfBlocks ; i++) {
            try {
                long currentSize = 0;
                while (currentSize <= sizeOfFile && (line = rd.readLine()) != null) {
                    box.add(line);
                    currentSize += line.length();
                }
                Collections.sort(box);
                File sortedFile = File.createTempFile("temp_", String.valueOf(i));
                PrintWriter wr = new PrintWriter(new OutputStreamWriter(new FileOutputStream(sortedFile)));
                for(String curLine: box){
                    wr.println(curLine);
                }
                box.clear();
                wr.close();
                pq.add(new BinaryFileBuffer(sortedFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        rd.close();

        PrintWriter output = new PrintWriter(new OutputStreamWriter(new FileOutputStream("res.txt")));
        try {
            while(pq.size() > 0) {
                BinaryFileBuffer binaryFileBuffer = pq.poll();
                String winner = binaryFileBuffer.pop();
                output.println(winner);
                if(binaryFileBuffer.empty()) {
                    binaryFileBuffer.br.close();
                    binaryFileBuffer.sourceFile.delete();
                } else {
                    pq.add(binaryFileBuffer);
                }
            }
        } finally {
            output.close();
            for(BinaryFileBuffer binaryFileBuffer : pq ) binaryFileBuffer.close();
        }

    }

    public static void main(String[] args) {
        try {
            if(args.length < 1){
                System.out.println("enter file path");
                return;
            }else{
                long time = System.currentTimeMillis();
                new ExternalSort().sort(args[0]);
                System.out.println("time=" + (System.currentTimeMillis() - time));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
