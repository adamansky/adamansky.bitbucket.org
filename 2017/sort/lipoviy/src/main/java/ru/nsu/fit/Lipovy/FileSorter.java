package ru.nsu.fit.Lipovy;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FileSorter {
    private String filePath;
    private String fileName;
    private int partsNumber;
    private static final int bufferSize = 1024 * 4;

    public FileSorter(String filePath, int partsNumber) {
        int index = filePath.lastIndexOf("/");

        if (index == -1) {
            this.filePath = "";
            this.fileName = filePath;
        } else {
            this.filePath = filePath.substring(0, index + 1);
            this.fileName = filePath.substring(index + 1);
        }

        this.partsNumber = partsNumber;
    }

    private void writePartInFile(List<byte[]> strings, Integer partNumber) throws IOException {
        OutputStream writer = new BufferedOutputStream(new FileOutputStream(filePath + "part" + partNumber.toString() + ".txt"), bufferSize);

        for (byte[] tmpBytes : strings) {
            writer.write(tmpBytes);
            writer.write('\n');
        }

        writer.close();

        return;
    }

    private long readAndSortPart(InputStream reader, long start, long end, List<byte[]> strings) throws IOException {
        long positionInFile = start;
        int readChars;

        while (positionInFile <= end) {
            byte tmpString[] = new byte[76];
            readChars = reader.read(tmpString);
            reader.read();

            positionInFile += readChars + 1;
            strings.add(tmpString);
        }

        strings.sort(new Comparator<byte[]>() {
            @Override
            public int compare(byte[] o1, byte[] o2) {
                int a, b;
                for (int i = 0, j = 0; i < o1.length && j < o2.length; i++, j++) {
                    a = (o1[i] & 0xff);
                    b = (o2[j] & 0xff);
                    if (a != b) {
                        return a - b;
                    }
                }
                return o1.length - o2.length;
            }
        });

        return positionInFile;
    }

    private int getStringIndex(List<String> currentStrings) {
        int index = 0;
        String s1, s2;
        for (int i = 1; i < currentStrings.size(); i++) {
            s1 = currentStrings.get(index);
            s2 = currentStrings.get(i);

            if (s1.compareTo(s2) > 0) {
                index = i;
            }
        }

        return index;
    }

    private void makeNewFile() throws IOException {
        List<BufferedReader> readers = new ArrayList<>();
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePath + "result.txt"), bufferSize);

        List<String> currentStrings = new ArrayList<>();
        for (Integer i = 0; i < partsNumber; i++) {
            BufferedReader tmpReader = new BufferedReader(new FileReader(filePath + "part" + i.toString() + ".txt"), bufferSize);
            readers.add(tmpReader);

            currentStrings.add(tmpReader.readLine());
        }

        int index;
        while (!currentStrings.isEmpty()) {
            index = getStringIndex(currentStrings);
            writer.write(currentStrings.get(index) + "\n");

//            if (index == currentStrings.size() - 1 && !lastPart.isEmpty()) {
//                String tmpString = new String(lastPart.get(0));
//                currentStrings.set(index, tmpString);
//                lastPart.remove(0);
//
//                if (lastPart.isEmpty()) {
//                    currentStrings.remove(currentStrings.size() - 1);
//                }
//            } else {
//                String tmpString = readers.get(index).readLine();
//                if (tmpString == null) {
//                    readers.get(index).close();
//                    readers.remove(index);
//                    currentStrings.remove(index);
//                } else {
//                    currentStrings.set(index, tmpString);
//                }
//            }

            String tmpString = readers.get(index).readLine();
            if (tmpString == null) {
                readers.get(index).close();
                readers.remove(index);
                currentStrings.remove(index);
            } else {
                currentStrings.set(index, tmpString);
            }

        }

        writer.close();

        return;
    }

    private void deleteTmpFiles() {
        for (Integer i = 0; i < partsNumber; i++) {
            File tmpFile = new File(filePath + "part" + i.toString() + ".txt");
            tmpFile.delete();
        }

        return;
    }

    public void start() throws IOException {
        long fileSize = new File(filePath + fileName).length();
        InputStream reader = new BufferedInputStream(new FileInputStream(filePath + fileName), bufferSize);

        long start = 0;
        long end = fileSize/partsNumber;

        for (int i = 0; i < partsNumber - 1; i++) {
            System.out.println("JVM Maxmem: " + (Runtime.getRuntime().freeMemory() >> 20) + " Mb");
            List<byte[]> strings = new ArrayList<>();

            end = fileSize*(i + 1)/partsNumber;
            start = readAndSortPart(reader, start, end, strings);

            writePartInFile(strings, i);
            System.out.println("JVM Maxmem: " + (Runtime.getRuntime().freeMemory() >> 20) + " Mb");
            System.out.println("=========================================================================================");
        }

        List<byte[]> lastPart = new ArrayList<>();
        long positionInFile = start;
        int readChars;
        end = fileSize;

        while (positionInFile <= end) {
            if (end - positionInFile < 76) {
                byte tmpString[] = new byte[(int)(end - positionInFile)];
                reader.read(tmpString);
                lastPart.add(tmpString);
                break;
            }

            byte tmpString[] = new byte[76];
            readChars = reader.read(tmpString);
            reader.read();

            positionInFile += readChars + 1;
            lastPart.add(tmpString);
        }

        reader.close();

        lastPart.sort(new Comparator<byte[]>() {
            @Override
            public int compare(byte[] o1, byte[] o2) {
                int a, b;
                for (int i = 0, j = 0; i < o1.length && j < o2.length; i++, j++) {
                    a = (o1[i] & 0xff);
                    b = (o2[j] & 0xff);
                    if (a != b) {
                        return a - b;
                    }
                }
                return o1.length - o2.length;
            }
        });

        writePartInFile(lastPart, partsNumber - 1);

        makeNewFile();
        deleteTmpFiles();

        return;
    }
}
